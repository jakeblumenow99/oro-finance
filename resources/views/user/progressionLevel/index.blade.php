@extends('guestLayout.view')

@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->

                <!--begin::Row-->
                <div class="row">
                    <div class="col-xl-12">
                        <!--begin::Charts Widget 2-->
                        <div class="card card-custom">
                            <div class="card-header">
                                <div class="card-title">
													<span class="card-icon">
														<i class="flaticon2-chat-1 text-primary"></i>
													</span>
                                    <h3 class="card-label">Welcome, {{ Auth::user()->name }}. Please choose an exam to
                                        start.</h3>
                                </div>

                            </div>
                            <div class="card-body">
                                <div class="row">


                                    @if(!empty($usersArray) && count($usersArray) > 0)
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>User Name</th>
                                                    <th>Email</th>
                                                    <th>Level</th>
                                                </tr>
                                                </thead>


                                                @if(!empty($usersArray) && count($usersArray) > 0)

                                                    @foreach($usersArray as $key=>$user)
                                                        @php
                                                            $level='';
                                                              $points = \App\Points::all();
                                                              foreach ($points as $point){
                                                                  if($point->points <= $progessionLevel[$key]){
                                                                      $level = $point->title;
                                                                      break;
                                                                  }else{
                                                                      $level = 'No Level';
                                                                  }
                                                              }

                                                        @endphp

                                                        <tr>
                                                            <td>{{$user->name}}</td>
                                                            <td>{{$user->email}}</td>
                                                            <td>{{$level}}</td>
                                                        </tr>

                                                    @endforeach
                                                @endif
                                            </table>

                                        </div>
                                    @endif

                                </div>

                            </div>


                        </div>
                        <!--end::Charts Widget 2-->
                    </div>

                </div>

            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->



@endsection


@section('pagination')

@endsection
