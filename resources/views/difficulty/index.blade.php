@extends('layouts.view')

@section('table_view')
    @if(!$difficultys->isEmpty())
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                        <th>Title</th>
                        <th>Points</th>
                        <th>Action</th>
                </tr>
                </thead>
                @foreach($difficultys as $difficulty)
                    <tr>
                        <td>{{$difficulty->title}}</td>
                        <td>{{$difficulty->points}}</td>
                        <td>
                            <a class="btn btn-warning" href="{{route('difficulty.edit', [$difficulty->id])}}">Edit</a>
                            <form method="post" action="{{ route('difficulty.destroy', $difficulty->id) }}"
                                  id="delete_{{ $difficulty->id }}" style="display: inline">
                                @method('delete')
                                @csrf
                                <a class="btn btn-danger" id="btn-delete-category"
                                   href="javascript:void(0)"
                                   onclick="document.getElementById('delete_<?=$difficulty->id?>').submit();">
                                    Delete
                                </a>
                            </form>
                        </td>

                    </tr>
                @endforeach

            </table>
        </div>
    @endif
@endsection

@section('pagination')
    {{$difficultys->links()}}
@endsection
