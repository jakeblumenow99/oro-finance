@extends('layouts.view')

@section('table_view')
    @include('errors.list')


    <form action="{{ route('difficulty.update',$difficulty->id) }}" method="post" class="form-horizontal">
        @csrf
        @method('patch')
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">Title</label>
            <div class="col-md-10">
                <input type="text" name="title" id="title" class="form-control" value="{{ $difficulty->title }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="points">Points</label>
            <div class="col-md-10">
                <input type="number" min="1" name="points" id="points" class="form-control" value="{{ $difficulty->points }}">
            </div>
        </div>


        <div class="form-group" style="    display: inline-flex;">

            <div class="col-sm-offset-2 col-sm-6">
                <button type="submit" class="btn btn-primary">Update Difficulty Level</button>

            </div>
            <div class="col-sm-offset-2 col-sm-6">
                <a class="btn btn-primary" href="{{route('difficulty.index')}}">Difficulty Level List</a>

            </div>

        </div>

    </form>
@endsection
@push('js')
    <script>
        $('#users').select2({
            placeholder: "Select Students",
            maximumInputLength: 20 // only allow terms up to 20 characters long
        });
        $('#status').on('click', function () {
            var value = $(this).val();
            if (value === "0") {
                $('#status').val(1);
            } else {
                $('#status').val(0);
            }
        });

    </script>
@endpush

