@extends('layouts.view')

@section('table_view')
    @include('errors.list')


    <form action="{{ route('category.update',$category->id) }}" method="post" class="form-horizontal">
        @csrf
        @method('patch')
        <div class="form-group">
            <label class="col-md-2 control-label" for="name">Name of category</label>
            <div class="col-md-10">
                <input type="text" name="name" id="name" value="{{ $category->name }}" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="status">Active</label>
            <div class="col-md-10">
                <input type="checkbox" name="status" id="status"
                       value="{{$category->status}}" {{ ($category->status == 1) ? 'checked' : '' }}>
            </div>
        </div>

        <div class="form-group" style="    display: inline-flex;">

            <div class="col-sm-offset-2 col-sm-6">
                <button type="submit" class="btn btn-primary">Update category</button>

            </div>
            <div class="col-sm-offset-2 col-sm-6" >
                <a class="btn btn-primary" href="{{route('category.index')}}">Category List</a>

            </div>

        </div>

    </form>
@endsection
@push('js')
    <script>
        $('#status').on('click',function () {
            var value = $(this).val();
            if(value === "0" ){
                $('#status').val(1);
            }else{
                $('#status').val(0);
            }
        });

    </script>
@endpush

