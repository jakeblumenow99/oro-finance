@extends('layouts.view')

@section('table_view')
    @include('errors.list')
    <form action="{{ route('category.store') }}" method="post" class="form-horizontal">
        @csrf
        <div class="form-group">
            <label class="col-md-2 control-label" for="name">Name of category</label>
            <div class="col-md-10">
                <input type="text" name="name" id="name"  class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="status">Active</label>
            <div class="col-md-10">
                <input type="checkbox" name="status" id="status" value="0">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Create category</button>

            </div>
        </div>

    </form>



@endsection
@push('js')
    <script>
        $('#status').on('click',function () {
            var value = $(this).val();
            if(value === "0" ){
                $('#status').val(1);
            }else{
                $('#status').val(0);
            }
        });

    </script>
@endpush

