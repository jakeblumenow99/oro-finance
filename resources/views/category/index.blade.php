@extends('layouts.view')

@section('table_view')
    @if(!$categories->isEmpty())
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>Category name</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->name}}</td>
                        <td>
                            <h4>
                                @if($category->status == 1)
                                    <span class="label label-success">Active
                             @else
                                            <span class="label label-warning">Inactivate


                                </span>
                                @endif
                            </h4>
                        </td>
                        <td>
                            <a class="btn btn-warning" href="{{route('category.edit', [$category->id])}}">Edit</a>
                            <form method="post" action="{{ route('category.destroy', $category->id) }}"
                                  id="delete_{{ $category->id }}" style="display: inline">
                                @method('delete')
                                @csrf
                                <a class="btn btn-danger" id="btn-delete-category"
                                   href="javascript:void(0)"
                                   onclick="document.getElementById('delete_<?=$category->id?>').submit();">
                                    Delete
                                </a>
                            </form>
{{--                            <a class="btn btn-danger" id="btn-delete-category"--}}
{{--                               href="{{route('category.destroy', [$category->id])}}">Delete</a>--}}
                        </td>

                    </tr>
                @endforeach

            </table>
        </div>
    @endif
@endsection

@section('pagination')
    {{$categories->links()}}
@endsection
