@extends('layouts.view')

@section('content')

    <div class="content-wrapper dashboard_tabs">
        <section class="content-header">
            <div class="container-fluid ">
                <div class="row mb-2">
                    <div class="col-sm-8">
                        <div class="col-sm-5 floting">
                            {{-- <h1>Profile Update</h1> --}}
                        </div>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="content">
            <div class="card card_cutom">
                <div class="card-header">
                    <h3 class="card-title">Profile Update</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{ route('user.profile.update') }}"
                          method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 floting">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" name="username" id="username"
                                           value="{{auth()->user()->name}}">
                                </div>
                            </div>
                            <div class="col-md-6 floting">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" id="email"
                                           value="{{auth()->user()->email}}">
                                </div>
                            </div>
                            <div class="col-md-4 floting">
                                <div class="form-group {{ $errors->has('current-password') ? ' has-error' : '' }}">
                                    <label for="current-password">Current
                                        Password</label>
                                    <input type="password" class="form-control"
                                           name="current-password"
                                           id="current-password">
                                    @if ($errors->has('current-password'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('current-password') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 floting">
                                <div class="form-group {{ $errors->has('new-password') ? ' has-error' : '' }}">
                                    <label for="new-password">New Password</label>
                                    <input type="password" class="form-control"
                                           name="new-password" id="new-password">
                                    @if ($errors->has('new-password'))
                                        <span class="help-block">
                                    <strong>{{ $errors->first('new-password') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4 floting">
                                <div class="form-group">
                                    <label for="new-password-confirm">Re-type
                                        Password</label>
                                    <input type="password" class="form-control"
                                           name="new-password-confirm"
                                           id="new-password-confirm">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mrgn no_padding">
                                <div class="col-md-3 floting"></div>

                                <div class="col-md-3 floting no_padding float-right">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-block btn-success btn_save">Update
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>


                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
@endsection
