@extends('layouts.view')

@section('table_view')
    @if(!$scores->isEmpty())
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                        <th>Title</th>
                        <th>Points</th>
                        <th>Action</th>
                </tr>
                </thead>
                @foreach($scores as $score)
                    <tr>
                        <td>{{$score->title}}</td>
                        <td>{{$score->points}}</td>
                        <td>
                            <a class="btn btn-warning" href="{{route('score.edit', [$score->id])}}">Edit</a>
                            <form method="post" action="{{ route('score.destroy', $score->id) }}"
                                  id="delete_{{ $score->id }}" style="display: inline">
                                @method('delete')
                                @csrf
                                <a class="btn btn-danger" id="btn-delete-category"
                                   href="javascript:void(0)"
                                   onclick="document.getElementById('delete_<?=$score->id?>').submit();">
                                    Delete
                                </a>
                            </form>
                        </td>

                    </tr>
                @endforeach

            </table>
        </div>
    @endif
@endsection

@section('pagination')
    {{$scores->links()}}
@endsection
