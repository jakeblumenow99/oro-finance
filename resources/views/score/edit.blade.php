@extends('layouts.view')

@section('table_view')
    @include('errors.list')


    <form action="{{ route('score.update',$score->id) }}" method="post" class="form-horizontal">
        @csrf
        @method('patch')
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">Title</label>
            <div class="col-md-10">
                <input type="text" name="title" id="title" class="form-control" value="{{ $score->title }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="points">Points</label>
            <div class="col-md-10">
                <input type="number" min="1" name="points" id="points" class="form-control" value="{{ $score->points }}">
            </div>
        </div>


        <div class="form-group" style="    display: inline-flex;">

            <div class="col-sm-offset-2 col-sm-6">
                <button type="submit" class="btn btn-primary">Update Score</button>

            </div>
            <div class="col-sm-offset-2 col-sm-6">
                <a class="btn btn-primary" href="{{route('score.index')}}">Score List</a>

            </div>

        </div>

    </form>
@endsection
@push('js')
    <script>

    </script>
@endpush

