@extends('layouts.view')

@section('table_view')
    @include('errors.list')
    <form action="{{ route('score.store') }}" method="post" class="form-horizontal">
        @csrf
        <div class="form-group">
            <label class="col-md-2 control-label" for="title">Title</label>
            <div class="col-md-10">
                <input type="text" name="title" id="title" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="points">Points</label>
            <div class="col-md-10">
                <input type="number" min="1" name="points" id="points" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Create Score</button>

            </div>
        </div>

    </form>



@endsection
@push('js')

@endpush

