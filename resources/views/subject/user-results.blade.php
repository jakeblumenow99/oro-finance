@extends('guestLayout.view')

@section('content')
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->

                <!--begin::Row-->
                <div class="row">
                    <div class="col-xl-12">
                        <!--begin::Charts Widget 2-->
                        <div class="card card-custom">
                            <div class="card-header">
                                <div class="card-title">
													<span class="card-icon">
														<i class="flaticon2-chat-1 text-primary"></i>
													</span>
                                    <h3 class="card-label">Welcome, {{ Auth::user()->name }}. Please choose an exam to
                                        start.</h3>
                                </div>

                            </div>
                            <div class="card-body">
                                <div class="row">


                                    @if(!empty($allsubject) && count($allsubject) > 0)
                                        @php
                                            $obtainedMarks = 0;
                                            $totalPercentage = 0;
                                            $subjectCount =0;
                                        @endphp
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Quiz Name</th>

                                                    <th>Total Question</th>
                                                    <th>Correct Answers</th>
                                                    <th>Total Marks(%)</th>
                                                    <th>Exam Date</th>
                                                </tr>
                                                </thead>



                                                @if(!empty($allsubject) && count($allsubject) > 0)

                                                    @foreach($allsubject as $subj)
                                                        @if(!empty($subj->answers) && count($subj->answers) > 0)
                                                            @php
                                                                $totalQuestions =count($subj->answers);
                                                                $count =0;
                                                                $examDate = null;
                                                                $subjectCount++;
                                                                $username = '';

                                                                foreach($subj->answers as $ans){
                                                                    $examDate = $ans->created_at;
                                                                    $username = $ans->user->name;

                                                                    if($ans->question_type ==1 || $ans->question_type == 3) {
                                                                        if($ans->user_answer == $ans->right_answer){
                                                                             $count++;
                                                                        }
                                                                    }
                                                                    if($ans->question_type ==2) {
                                                                        if($ans->user_answerB == $ans->answerB){
                                                                             $count =$count + 0.5;
                                                                        }
                                                                        if($ans->user_answerA == $ans->answerA){
                                                                              $count =$count + 0.5;

                                                                        }

                                                                    }
                                                                }
                                                                $percentage = ($count/$totalQuestions)*100;
                                                                $obtainedMarks = $obtainedMarks + $percentage;

                                                            @endphp
                                                            <tr>
                                                                <td>{{$subj->name}}</td>
                                                                <td>{{$totalQuestions}}</td>
                                                                <td>{{$count}}</td>
                                                                <td>{{ceil($percentage)}}%</td>
                                                                <td>{{$examDate}}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </table>
                                            <table class="table table-hover table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Total Marks</th>
                                                </tr>
                                                </thead>
                                                @php
                                                    if($subjectCount != 0){
                                                        $totalPercentage = ($obtainedMarks/$subjectCount);
                                                    }

                                                @endphp

                                                <tr>
                                                    <td>{{ceil($totalPercentage)}}%</td>

                                                </tr>

                                            </table>
                                        </div>
                                    @endif

                                </div>

                            </div>


                        </div>
                        <!--end::Charts Widget 2-->
                    </div>

                </div>

            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->


@endsection


@section('pagination')

@endsection
