@extends('layouts.view')

@section('table_view')
    @include('errors.list')
    <form action="{{ route('subject.new') }}" method="post" class="form-horizontal">
        @csrf
        <div class="form-group">
            <label class="col-md-2 control-label" for="name">Name of subject</label>
            <div class="col-md-7">
                <input type="text" name="name" id="name" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="categories">Categories</label>
            <div class="col-md-5">
                <select class="form-control" name="categories[]" id="categories"   multiple="multiple">
{{--                    <option value="">Select Category</option>--}}
                    @if($categories)
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}"> {{ ucfirst($category->name) }}</option>
                        @endforeach
                    @endif

                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="classes">Classes</label>
            <div class="col-md-5">
                <select class="form-control" name="classes" id="classes">
                    {{--                    <option value="">Select Category</option>--}}
                    @if($classes)
                        @foreach($classes as $class)
                            <option value="{{ $class->id }}"> {{ ucfirst($class->name) }}</option>
                        @endforeach
                    @endif

                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="duration">Duration</label>
            <div class="col-md-4">
                <div class="input-group">
                    <input type="text" name="duration" id="duration" class="form-control">
                    <span class="input-group-addon">MINUTES</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="question_limit">Question limit</label>
            <div class="col-md-4">
                <div class="input-group">
                    <input type="text" name="question_limit" id="question_limit" class="form-control">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="status">Active</label>
            <div class="col-md-10">
                <input type="checkbox" name="status" id="status" value="0">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Create subject</button>

            </div>
        </div>

    </form>
@endsection
@push('js')
{{--    <script src="{{asset('js/select2.js')}}"></script>--}}
    <script>
        $('#categories').select2({
            placeholder: "Select Category",
            maximumInputLength: 20 // only allow terms up to 20 characters long
        });

        $('#classes').select2({
            placeholder: "Select Classes",
            maximumInputLength: 20 // only allow terms up to 20 characters long
        });
        $('#status').on('click', function () {
            var value = $(this).val();
            if (value === "0") {
                $('#status').val(1);
            } else {
                $('#status').val(0);
            }
        });

    </script>
@endpush


