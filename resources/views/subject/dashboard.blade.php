@extends('guestLayout.view')

@section('content')


    <!--begin::Content-->

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">

            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <div class="row">
                    <div class="col-xl-12">
                        <div class="alert alert-custom alert-notice alert-light-primary fade show mb-5"
                             style="background-color: #ebf8ff" role="alert">
                            <div class="alert-icon">
                                <i class="flaticon2-open-text-book"></i>
                            </div>
                            <div class="alert-text"><span class="bold">Welcome to Oro!</span> Your tests will include 10
                                questions (5 MCQs and 5 ‘fill in the gap’ questions). You have 30 minutes to complete
                                each test and one attempt.
                            </div>
                            <div class="alert-close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	<span aria-hidden="true">
																		<i class="ki ki-close"></i>
																	</span>
                                </button>
                            </div>
                        </div>


                    </div>
                </div>
                <!--begin::Row-->
                <div class="row">
                    <div class="col-xl-8">
                        <!--begin::Charts Widget 2-->

                        <div class="card shadow fade show card-custom card-stretch gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="card-label font-weight-bolder text-dark">My Tests</span>
                                    <span class="text-muted mt-3 font-weight-bold font-size-sm">User: <span
                                            style="text-transform: uppercase"> {{ $user->name }} </span>| Module Code: BS1196</span>
                                </h3>
                                <div class="card-toolbar">
                                    <ul class="nav nav-pills nav-pills-sm nav-dark-75">
                                        <li class="nav-item">
                                            <a class="nav-link py-2 px-4 active" data-toggle="tab"
                                               href="#kt_tab_pane_1_1">Term 1</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link py-2 px-4 disabled" data-toggle="tab"
                                               href="#kt_tab_pane_1_2">Term 2</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link py-2 px-4 disabled" data-toggle="tab"
                                               href="#kt_tab_pane_1_2">Term 3</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body pt-2 pb-0">
                                <!--begin::Table-->
                                <div class="table-responsive">
                                    <table class="table table-borderless table-vertical-center">
                                        <thead>
                                        <tr>
                                            <th class="p-0" style="width: 50px"></th>
                                            <th class="p-0" style="min-width: 150px"></th>
                                            <th class="p-0" style="min-width: 140px"></th>
                                            <th class="p-0" style="min-width: 100px"></th>
                                            <th class="p-0" style="min-width: 40px"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="pl-0 py-5">
                                                <div class="symbol symbol-45 symbol-light-primary mr-2">
																		<span
                                                                            class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\Design\PenAndRuller.svg--><svg
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                width="24px" height="24px"
                                                                                viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path
            d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z"
            fill="#000000" opacity="0.3"/>
        <path
            d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z"
            fill="#000000"/>
    </g>
</svg><!--end::Svg Icon--></span>
                                                </div>
                                            </td>

                                            <td class="pl-0">
                                                <span
                                                    class="text-muted font-weight-bold">Your test(s) will appear here</span>
                                            </td>
                                            <td class="text-right">
                                                <span class="text-muted font-weight-bold">1 attempt left</span>
                                            </td>
                                            <td class="text-right">
                                                <span class="text-muted font-weight-bold">30 minutes</span>
                                            </td>
                                            <td class="text-right">
                                                <span class="text-muted font-weight-bold">10 questions</span>
                                            </td>

                                        </tr>
                                        @foreach(App\Subject::with('category','category.questions')->get() as $cat)

                                            @php
                                                $questions = \App\CatQuestion::where('category_id',$cat->category->id)->get();
                                            @endphp
                                            @if( !empty($questions) && count($questions) > 0)
                                                @php
                                                    $takenQuiz = \App\Answer::where('user_id',auth()->user()->id)->where('subject_id',$cat->id)->groupBy('subject_id')->get();
                                                    $checkUserClass = \App\ClassUser::where('class_id',$cat->classes->id)->where('user_id',auth()->user()->id)->get();
                                                @endphp
                                                @if( count($takenQuiz) > 0)
                                                @elseif(count($checkUserClass) > 0)
                                                    <tr>
                                                        <td class="pl-0 py-5">
                                                            <div class="symbol symbol-45 symbol-light-primary mr-2">
																		<span
                                                                            class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\Design\PenAndRuller.svg--><svg
                                                                                xmlns="http://www.w3.org/2000/svg"
                                                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                width="24px" height="24px"
                                                                                viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path
            d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z"
            fill="#000000" opacity="0.3"/>
        <path
            d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z"
            fill="#000000"/>
    </g>
</svg><!--end::Svg Icon--></span>
                                                            </div>
                                                        </td>

                                                        <td class="pl-0">
                                                            <a href="{{route('before-exam',$cat->id)}}"
                                                               class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$cat->name}}</a>
                                                        </td>
                                                        <td class="text-right">
                                                            <span
                                                                class="text-muted font-weight-bold">1 attempt left</span>
                                                        </td>
                                                        <td class="text-right">
                                                            <span class="text-muted font-weight-bold">30 minutes</span>
                                                        </td>
                                                        <td class="text-right">
                                                            <span
                                                                class="text-muted font-weight-bold">10 questions</span>
                                                        </td>
                                                        <td class="text-right pr-0">
                                                            <a href="{{route('before-exam',$cat->id)}}"
                                                               class="btn btn-icon btn-light btn-sm">
																				<span
                                                                                    class="svg-icon svg-icon-md svg-icon-success">
																					<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Arrow-right.svg-->
																					<svg
                                                                                        xmlns="http://www.w3.org/2000/svg"
                                                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                        width="24px" height="24px"
                                                                                        viewBox="0 0 24 24"
                                                                                        version="1.1">
																						<g stroke="none"
                                                                                           stroke-width="1" fill="none"
                                                                                           fill-rule="evenodd">
																							<polygon
                                                                                                points="0 0 24 0 24 24 0 24"></polygon>
																							<rect fill="#000000"
                                                                                                  opacity="0.3"
                                                                                                  transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)"
                                                                                                  x="11" y="5" width="2"
                                                                                                  height="14"
                                                                                                  rx="1"></rect>
																							<path
                                                                                                d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z"
                                                                                                fill="#000000"
                                                                                                fill-rule="nonzero"
                                                                                                transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
																						</g>
																					</svg>
                                                                                    <!--end::Svg Icon-->
																				</span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!--end::table-->
                            </div>
                            <!--begin::Body-->
                        </div>                  <!--end::Charts Widget 2-->
                    </div>
                    <div class="col-xl-4">
                        <div class="card shadow fade show card-custom">
                            <div class="card-header">
                                <div class="card-title">

                                    <h3 class="card-label">Oro Calender</h3>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="timeline timeline-5">
                                    <div class="timeline-items">
                                        <!--begin::Item-->
                                        <div class="timeline-item">
                                            <!--begin::Icon-->
                                            <div class="timeline-media bg-light-success">
                <span class="svg-icon-primary svg-icon-md">
               <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\General\User.svg--><svg
                       xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                       height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path
            d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z"
            fill="#000000" fill-rule="nonzero" opacity="0.3"/>
        <path
            d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z"
            fill="#000000" fill-rule="nonzero"/>
    </g>
</svg><!--end::Svg Icon--></span>
                </span>
                                            </div>
                                            <!--end::Icon-->

                                            <!--begin::Info-->
                                            <div class="timeline-desc timeline-desc-light-primary">
                                                <span class="font-weight-bolder text-primary">21st December</span>
                                                <p class="font-weight-normal text-dark-50 pb-2">
                                                    Practice tests will be available for you to familiarise yourself
                                                    with the type of questions. They do not contribute towards your
                                                    mark. </p>
                                            </div>
                                            <!--end::Info-->
                                        </div>
                                        <!--end::Item-->

                                        <!--begin::Item-->
                                        <div class="timeline-item">
                                            <!--begin::Icon-->
                                            <div class="timeline-media bg-light-warning">
                <span class="svg-icon-warning svg-icon-md">
                <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\Devices\Router2.svg--><svg
                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                        height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <rect fill="#000000" x="3" y="13" width="18" height="7" rx="2"/>
        <path
            d="M17.4029496,9.54910207 L15.8599014,10.8215022 C14.9149052,9.67549895 13.5137472,9 12,9 C10.4912085,9 9.09418404,9.67104182 8.14910121,10.8106159 L6.60963188,9.53388797 C7.93073905,7.94090645 9.88958759,7 12,7 C14.1173586,7 16.0819686,7.94713944 17.4029496,9.54910207 Z M20.4681628,6.9788888 L18.929169,8.25618985 C17.2286725,6.20729644 14.7140097,5 12,5 C9.28974232,5 6.77820732,6.20393339 5.07766256,8.24796852 L3.54017812,6.96885102 C5.61676443,4.47281829 8.68922234,3 12,3 C15.3153667,3 18.3916375,4.47692603 20.4681628,6.9788888 Z"
            fill="#000000" fill-rule="nonzero" opacity="0.3"/>
    </g>
</svg><!--end::Svg Icon--></span>
                </span>
                                            </div>
                                            <!--end::Icon-->

                                            <!--begin::Info-->
                                            <div class="timeline-desc timeline-desc-light-warning">
                                                <span class="font-weight-bolder text-warning">4th January</span>
                                                <p class="font-weight-normal text-dark-50 pt-1 pb-2">
                                                    Test one will be made available until 11 pm on Monday 11th January
                                                    2021.
                                                </p>
                                            </div>
                                            <!--end::Info-->
                                        </div>
                                        <!--end::Item-->

                                        <!--begin::Item-->
                                        <div class="timeline-item">
                                            <!--begin::Icon-->
                                            <div class="timeline-media bg-light-danger">
                <span class="svg-icon-success svg-icon-md">
               <span class="svg-icon svg-icon-primary svg-icon-2x"> <span class="svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo5\dist/../src/media/svg/icons\Code\Stop.svg--><svg
                           xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                           height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path
            d="M12,22 C6.4771525,22 2,17.5228475 2,12 C2,6.4771525 6.4771525,2 12,2 C17.5228475,2 22,6.4771525 22,12 C22,17.5228475 17.5228475,22 12,22 Z M12,20 C16.418278,20 20,16.418278 20,12 C20,7.581722 16.418278,4 12,4 C7.581722,4 4,7.581722 4,12 C4,16.418278 7.581722,20 12,20 Z M19.0710678,4.92893219 L19.0710678,4.92893219 C19.4615921,5.31945648 19.4615921,5.95262146 19.0710678,6.34314575 L6.34314575,19.0710678 C5.95262146,19.4615921 5.31945648,19.4615921 4.92893219,19.0710678 L4.92893219,19.0710678 C4.5384079,18.6805435 4.5384079,18.0473785 4.92893219,17.6568542 L17.6568542,4.92893219 C18.0473785,4.5384079 18.6805435,4.5384079 19.0710678,4.92893219 Z"
            fill="#000000" fill-rule="nonzero" opacity="0.3"/>
    </g>
</svg><!--end::Svg Icon--></span><!--end::Svg Icon--></span>
                </span>
                                            </div>

                                            <!--end::Icon-->

                                            <!--begin::Info-->
                                            <div class="timeline-desc timeline-desc-light-success">
                                                <span class="font-weight-bolder text-success">11th January</span>
                                                <p class="font-weight-normal text-dark-50 pt-1 pb-2">
                                                    Test two will be made available until 11 pm on Monday 18th January
                                                    2021. </p>
                                            </div>
                                            <!--end::Info-->
                                        </div>
                                        <!--end::Item-->

                                        <!--begin::Item-->
                                        <!--end::Item-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

@endsection
