@extends('layouts.view')

@section('table_view')
    @if(!$subjects->isEmpty())
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>Subject Name</th>
                    <th>Category</th>
                    <th>Class</th>
                    <th>Duration</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                @foreach($subjects as $subject)
                    <tr>
                        <td>{{$subject->name}}</td>
                        <td>{{$subject->category['name']}}</td>
                        <td>{{$subject->classes['name']}}</td>
                        <td>{{$subject->duration}} mins</td>
                        <td>
                            <h4>
                                @if($subject->status == 1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-warning">Inactivate</span>
                                @endif


                            </h4>
                        </td>
                        <td>
                            <a class="btn btn-info" href="{{route('subject.question', [$subject->id])}}">Manage
                                Questions</a>
                            <a class="btn btn-warning"
                               href="{{route('subject.question.edit', [$subject->id])}}">Edit</a>

                            <form method="get" action="{{ route('subject.delete', $subject->id) }}"
                                  id="delete_{{ $subject->id }}" style="display: inline">
                                @csrf
                                <a class="btn btn-danger" id="btn-delete-category"
                                   href="javascript:void(0)"
                                   onclick="document.getElementById('delete_<?=$subject->id?>').submit();">
                                    Delete
                                </a>
                            </form>

                        </td>

                    </tr>
                @endforeach

            </table>
        </div>
    @endif
@endsection

@section('pagination')
    {{$subjects->links()}}
@endsection
