@extends('layouts.view')

@section('table_view')
    <div class="row">
        <div class="col-md-offset-1 col-md-7">
            <h3>Subject name: {{$subject->name}}</h3>
            <h3>Category: {{$subject->category->name}}</h3>
            <h3>Duration: {{$subject->duration}} minutes</h3>

        </div>

    </div>
    <br>
    <a class="btn btn-warning" href="{{route('subject.index')}}"><span class="glyphicon glyphicon-arrow-left"></span>
        Back to subjects</a>

    <!-- <button type="button" class="btn btn-primary" id="btn-add-new-question"><span
            class="glyphicon glyphicon-plus"></span> Add new question
    </button> -->

{{--    <div class="form-group">--}}
{{--        <label class="col-md-2 control-label" for="answer">Choose Questions</label>--}}
{{--        <div class="col-md-7">--}}
{{--                @if($answer)--}}
{{--                    @foreach($questions_tab as $question)--}}
{{--                        <p>{{ $question->question }}</p>--}}
{{--                    @endforeach--}}
{{--                @endif--}}

{{--        </div>--}}
{{--    </div>--}}
    <br><br><br>

    @include('errors.list')

    <form action="{{ route('subject.post.new.question',$subject->id) }}" method="post" class="form-horizontal"
          id="add-new-question">

        @include('subject.formquestion')

        @if(!$questions->isEmpty())
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Questions added</div>

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php $i = 0;
                    ?>
                    @foreach($questions as $question)

                        <?php $i++;?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading{{$question->question_id}}">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse{{$question->question_id}}" aria-expanded="false"
                                       aria-controls="collapse{{$question->question_id}}">
                                        Question #{{$i}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{$question->question_id}}" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="heading{{$question->question_id}}">
                                <div class="panel-body">
                                    <form action="{{ route('subject.question.post.edit',$question->question_id) }}" method="post"
                                          class="form-horizontal" id="add-new-question">
                                    @include('subject.formquestion')

                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
    @endif
@stop
