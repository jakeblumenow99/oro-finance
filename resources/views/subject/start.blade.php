@extends('guestLayout.view')

@section('content')

    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->

                <!--begin::Row-->
                <div class="row">
                    <div class="col-xl-12">
                        <!--begin::Charts Widget 2-->
                        <div class="card card-custom">
                            <div class="card-header">
                                <div class="card-title">
													<span class="card-icon">
														<i class="flaticon2-chat-1 text-primary"></i>
													</span>
                                    <h3 class="card-label">Welcome, {{ Auth::user()->name }}. Please choose an exam to
                                        start.</h3>
                                </div>

                            </div>
                            <div class="card-body">
                                <div class="row">


                                    <div class="jumbotron" style="width: 100%">
                                        <h3>Subject Name: <b>{{$subject->name}}</b></h3>
                                        <h3>Duration: <b>{{$subject->duration}}</b></h3>

                                        <a class="btn btn-success btn-lg" href="{{route('start-exam',$subject->id)}}"
                                           role="button">START EXAM</a>
                                    </div>

                                </div>

                            </div>


                        </div>
                        <!--end::Charts Widget 2-->
                    </div>

                </div>

            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->




@endsection
