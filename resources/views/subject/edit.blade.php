@extends('layouts.view')

@section('table_view')
    @include('errors.list')
    <form action="{{ route('subject.question.patch.edit',$subject->id) }}" method="post" class="form-horizontal">
        @csrf
        @method('patch')
        <div class="form-group">
            <label class="col-md-2 control-label" for="name">Name of subject</label>
            <div class="col-md-7">
                <input type="text" name="name" id="name" class="form-control" value="{{$subject->name}}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="categories">Categories</label>
            <div class="col-md-5">
                <select class="form-control" name="categories" id="categories">
                    <option value="">Select Category</option>
                    @if($categories)
                        @foreach($categories as $category)
                            <option
                                value="{{ $category->id }}" {{ ($category->id == $selectedCategoryId ) ? 'selected' : '' }}> {{ ucfirst($category->name) }}</option>
                        @endforeach
                    @endif

                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label" for="duration">Duration</label>
            <div class="col-md-4">
                <div class="input-group">
                    <input type="text" name="duration" id="duration" class="form-control" value="{{ $subject->duration }}">
                    <span class="input-group-addon">MINUTES</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="question_limit">Question limit</label>
            <div class="col-md-4">
                <div class="input-group">
                    <input type="text" name="question_limit" id="question_limit" class="form-control" value="{{ $subject->question_limit }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="status">Active</label>
            <div class="col-md-10">
                <input type="checkbox" name="status" id="status"  value="{{$subject->status}}" {{ ($subject->status == 1) ? 'checked' : '' }}>
            </div>
        </div>

        <div class="form-group" style="    display: inline-flex;">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Update subject</button>

            </div>
            <div class="col-sm-offset-2 col-sm-6" >
                <a class="btn btn-primary" href="{{route('subject.index')}}">Subject List</a>

            </div>
        </div>

    </form>
@endsection
@push('js')
    <script>
        $('#status').on('click', function () {
            var value = $(this).val();
            if (value === "0") {
                $('#status').val(1);
            } else {
                $('#status').val(0);
            }
        });

    </script>
@endpush


