@csrf
<div class="form-group">
    <label for="question" class="col-md-3 control-label" style="text-align: left">Question</label>
    <div class="col-md-9">
        <textarea name="question" id="question" class="form-control" rows="5"
                  cols="5">{{ $question->question ?? '' }} {{ $question->question_other ?? '' }}</textarea>
    </div>
</div>
@if(isset($question) && !is_null($question->graph_image))
    <div class="form-group">
        <div class="col-md-12">
            <img src="{{asset('storage/images/'.$question->graph_image) }}" style="width: 100%">
        </div>
    </div>
@endif

<div class="form-group">
    <label for="option1" class="col-md-3 control-label" style="text-align: left">Option #A</label>
    <div class="col-md-9">
        <input type="text" name="optiona" id="optiona" class="form-control"
               value="{{$question->optionA ?? ''}}">
    </div>

</div>

<div class="form-group">
    <label for="option2" class="col-md-3 control-label" style="text-align: left">Option #B</label>
    <div class="col-md-9">
        <input type="text" name="optionb" id="optionb" class="form-control"
               value="{{$question->optionB ?? ''}}">
    </div>

</div>

@if(isset($question))
    <div class="forMcqs"
         style="display: {{$question->question_type == 1 || $question->question_type == 3   ? '' : 'none'}}">
        <div class="form-group">
            <label class="col-md-3 control-label" for="question" style="text-align: left">Option #C</label>
            <div class="col-md-9">
                <input type="text" name="optionc" id="optionc" class="form-control"
                       value="{{$question->optionC}}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="question" style="text-align: left">Option #D</label>
            <div class="col-md-9">
                <input type="text" name="optiond" id="optiond" class="form-control"
                       value="{{$question->optionD}}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="answer" style="text-align: left">Answer</label>
            <div class="col-md-9">
                <select class="form-control" name="answer" id="questiontype">
                    <option value=""> Select right answer</option>
                    <option value="1" {{ $question->Answer == 1 ? 'selected' : '' }}> Option A</option>
                    <option value="2" {{ $question->Answer == 2 ? 'selected' : '' }}> Option B</option>
                    <option value="3" {{ $question->Answer == 3 ? 'selected' : '' }}> Option C</option>
                    <option value="4" {{ $question->Answer == 4 ? 'selected' : '' }}> Option D</option>
                </select>
            </div>
        </div>
    </div>

<div class="forFillInTheBlanks" style="display: {{$question->question_type == 2  ? '' : 'none'}}">
    <div class="form-group">
        <label class="col-md-3 control-label" for="answerA" style="text-align: left">Type Right Answer A</label>
        <div class="col-md-9">
            <input type="text" name="answerA" id="answerA" class="form-control"
                   value="{{ $question->answerA}}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label" for="answerB" style="text-align: left">Type Right Answer B</label>
        <div class="col-md-9">
            <input type="text" name="answerB" id="answerB" class="form-control"
                   value="{{ $question->answerB}}">
        </div>
    </div>

</div>
@endif


<div class="form-group">
    <label class="col-md-3 control-label" for="difficulty_level_id" style="text-align: left">Difficulty Levels</label>
    <div class="col-md-9">
        <select class="form-control" name="difficulty_level_id" id="difficulty_level_id">
            <option value="">Select Answer</option>
            @if($difficultyLevels)
                @foreach($difficultyLevels as $difficultyLevel)
                    <option
                        value="{{ $difficultyLevel->id }}" {{ (isset($question) && !is_null($question->difficultyLevel) && $question->difficultyLevel->id == $difficultyLevel->id) ? 'selected' : '' }}> {{ ucfirst($difficultyLevel->title) }}</option>
                @endforeach
            @endif

        </select>
    </div>
</div>

<div class="form-group">
    {{--    <div class="col-sm-offset-2 col-sm-2">--}}
    {{--        <button type="submit" class="btn btn-primary">{{ $title_button }}</button>--}}
    {{--    </div>--}}
    {{--    @if(isset($question))--}}
    {{--        {{ dd($question) }}--}}
    {{--        <div class="col-sm-offset-1 col-sm-2">--}}
    {{--            <a class="btn btn-danger" id="btn-delete"--}}
    {{--               href="{{route('subject.question.delete', [$question->question_id])}}">Delete</a>--}}

    {{--        </div>--}}
    {{--    @endif--}}
</div>
</form>

