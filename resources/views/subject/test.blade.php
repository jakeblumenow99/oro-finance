@extends('guestLayout.view')
@section('content')
    @push('css')
        <style>
            .base-timer {
                position: relative;
                width: 100px;
                height: 100px;
                float: right;
            }

            .base-timer__svg {
                transform: scaleX(-1);
            }

            .base-timer__circle {
                fill: none;
                stroke: none;
            }

            .base-timer__path-elapsed {
                stroke-width: 7px;
                stroke: grey;
            }

            .base-timer__path-remaining {
                stroke-width: 7px;
                stroke-linecap: round;
                transform: rotate(90deg);
                transform-origin: center;
                transition: 1s linear all;
                fill-rule: nonzero;
                stroke: currentColor;
            }

            .base-timer__path-remaining.green {
                color: rgb(65, 184, 131);
            }

            .base-timer__path-remaining.orange {
                color: orange;
            }

            .base-timer__path-remaining.red {
                color: red;
            }

            .base-timer__label {
                position: absolute;
                width: 50px;
                height: 50px;
                top: 23px;
                display: flex;
                align-items: center;
                justify-content: center;
                font-size: 25px;
                left: 23px;
            }

        </style>
    @endpush
    @push('js')
        <script>
            $(function () {
                const FULL_DASH_ARRAY = 283;
                const WARNING_THRESHOLD = 10;
                const ALERT_THRESHOLD = 5;

                const COLOR_CODES = {
                    info: {
                        color: "green"
                    },
                    warning: {
                        color: "orange",
                        threshold: WARNING_THRESHOLD
                    },
                    alert: {
                        color: "red",
                        threshold: ALERT_THRESHOLD
                    }
                };

                const TIME_LIMIT = {{$duration*60}};
                let timePassed = 0;
                let timeLeft = TIME_LIMIT;
                let timerInterval = null;
                let remainingPathColor = COLOR_CODES.info.color;

                document.getElementById("counter1").innerHTML = `
                                <div class="base-timer">
                                  <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                    <g class="base-timer__circle">
                                      <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45"></circle>
                                      <path
                                        id="base-timer-path-remaining"
                                        stroke-dasharray="283"
                                        class="base-timer__path-remaining ${remainingPathColor}"
                                        d="
                                          M 50, 50
                                          m -45, 0
                                          a 45,45 0 1,0 90,0
                                          a 45,45 0 1,0 -90,0
                                        "
                                      ></path>
                                    </g>
                                  </svg>
                                  <span id="base-timer-label" class="base-timer__label">${formatTime(
                    timeLeft
                )}</span>
                                </div>
                                `;

                startTimer();

                function onTimesUp() {
                    clearInterval(timerInterval);
                }

                function startTimer() {
                    timerInterval = setInterval(() => {
                        timePassed = timePassed += 1;
                        timeLeft = TIME_LIMIT - timePassed;
                        document.getElementById("base-timer-label").innerHTML = formatTime(
                            timeLeft
                        );

                        @foreach($questions as $q)
                        $('#time_taken{{$q->question_id}}').val(timeLeft);
                        @endforeach
                        setCircleDasharray();
                        setRemainingPathColor(timeLeft);

                        if (timeLeft === 0) {
                            onTimesUp();
                            alert("The time has run out!");
                            window.location.href = '{{ route('index.home') }}';
                        }
                    }, 1000);
                }

                function formatTime(time) {
                    const minutes = Math.floor(time / 60);
                    let seconds = time % 60;

                    if (seconds < 10) {
                        seconds = `0${seconds}`;
                    }

                    return `${minutes}:${seconds}`;
                }

                function setRemainingPathColor(timeLeft) {
                    const {alert, warning, info} = COLOR_CODES;
                    if (timeLeft <= alert.threshold) {
                        document
                            .getElementById("base-timer-path-remaining")
                            .classList.remove(warning.color);
                        document
                            .getElementById("base-timer-path-remaining")
                            .classList.add(alert.color);
                    } else if (timeLeft <= warning.threshold) {
                        document
                            .getElementById("base-timer-path-remaining")
                            .classList.remove(info.color);
                        document
                            .getElementById("base-timer-path-remaining")
                            .classList.add(warning.color);
                    }
                }

                function calculateTimeFraction() {
                    const rawTimeFraction = timeLeft / TIME_LIMIT;
                    return rawTimeFraction - (1 / TIME_LIMIT) * (1 - rawTimeFraction);
                }

                function setCircleDasharray() {
                    const circleDasharray = `${(
                        calculateTimeFraction() * FULL_DASH_ARRAY
                    ).toFixed(0)} 283`;
                    document
                        .getElementById("base-timer-path-remaining")
                        .setAttribute("stroke-dasharray", circleDasharray);
                }




                {{--var clock = $('#counter1').FlipClock({{$duration*60}}, {--}}
                {{--    autoStart: false,--}}
                {{--    countdown: true,--}}
                {{--    clockFace: 'MinuteCounter',--}}
                {{--    callbacks: {--}}
                {{--        interval: function () {--}}
                {{--            var time = clock.getTime().time;--}}
                {{--            //alert(time);--}}
                {{--            @foreach($questions as $q)--}}
                {{--            $('#time_taken{{$q->question_id}}').val(time);--}}
                {{--            @endforeach--}}
                {{--        },--}}
                {{--        stop: function () {--}}
                {{--            alert("The time has run out!");--}}
                {{--            window.location.href = '{{ route('index.home') }}';--}}
                {{--        }--}}

                {{--    }--}}
                {{--});--}}
                {{--clock.start();--}}
            })
        </script>
    @endpush
    <style>
        .jumbotron {
            background: lightgray !important;


        }

        .threeq {


        }

        .btn.btn-primary:hover:not(.btn-text):not(:disabled):not(.disabled), .btn.btn-primary:focus:not(.btn-text), .btn.btn-primary.focus:not(.btn-text) {
            color: #FFFFFF !important;
            background-color: #ffc107 !important;
            border-color: #ffffff !important;
        }

        .btn.btn-primary:not(:disabled):not(.disabled):active:not(.btn-text), .btn.btn-primary:not(:disabled):not(.disabled).active, .show > .btn.btn-primary.dropdown-toggle, .show .btn.btn-primary.btn-dropdown {
            color: #FFFFFF !important;
            background-color: #009688 !important;
            border-color: #ffffff !important;
        }
    </style>
    <p id="counter1"></p>

    <div class="content d-flex flex-column flex-column-fluid" id="kt_content" style="padding:20px!important;">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Dashboard-->
                <!--begin::Row-->
                <div class="row">
                    <div class="col-xl-12">
                        <!--begin::Charts Widget 2-->
                        <div class="card card-custom">

                            @foreach($questions as $question)
                                <div class="card-body threeq" id="jake{{$question->question_id}}"
                                     @if($question->question_id != $current_question_id)
                                     style="display: none; "
                                    @endif
                                >
                                    @if(!is_null($question->graph_image))
                                        <img src="{{asset('storage/images/'.$question->graph_image) }}"
                                             style="width: 100%;">
                                    @endif
                                    <form action="{{ route('user.save-question', $subject->id) }}" method="POST"
                                          id="frm{{$question->question_id}}">
                                        @csrf
                                        <input type="hidden" name="question_id" value="{{$question->question_id}}">
                                        <input id="time_taken{{$question->question_id}}"
                                               name="time_taken{{$question->question_id}}"
                                               type="hidden">
                                        <input id="question_type" name="question_type"
                                               value="{{$question->question_type}}"
                                               type="hidden">
                                        {{--                                            <p>QID:{{$question->question_id}}</p>--}}
                                        @if($question->question_type == 1 ||  $question->question_type == 3)

                                            <div class="mb-6">
                                                <div class="progress" style="height: 20px;">
                                                    <div class="progress-bar" role="progressbar" style="width: 25%;"
                                                         aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="row">


                                                <div class="card card-custom mb-6">
                                                    <div class="card-body rounded p-0 d-flex"
                                                         style="background-color:#DAF0FD;">

                                                        <div
                                                            class="d-flex flex-column flex-lg-row-auto w-auto p-10 p-md-20">
                                                            <h1 class="font-weight-bolder text-dark"><p
                                                                    style="width: 80vw;"> {{ $question->question }}</p>
                                                            </h1>


                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <style>
                                                .span {
                                                    opacity: 0 !important;
                                                    position: fixed !important;
                                                    width: 0 !important;
                                                }


                                            </style>
                                            <div class="mb-11">

                                                <div class="row" id="answer-radio{{$question->question_id}}">

                                                    <div class="btn-group" data-toggle="buttons"
                                                         style="display: contents!important;">
                                                        <div class="col-md-3 col-xxl-3 col-lg-12">

                                                            <div class="card card-custom card-shadowless">
                                                                <label class="btn btn-primary">
                                                                    <input class="span" type="radio" name="option"
                                                                           value="1">
                                                                    <span class="span"></span>
                                                                    <div class="card-body p-0">

                                                                        <div
                                                                            class="flex-grow-1 bg-danger p-12 pb-40 card-rounded flex-grow-1 bgi-no-repeat">

                                                                            <p class="text-inverse-danger pt-10 pb-5 font-size-h3 font-weight-bolder line-height-lg">{{$question->optionA}}
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-xxl-3 col-lg-12">

                                                            <div class="card card-custom card-shadowless">
                                                                <label class="btn btn-primary">
                                                                    <input class="span" type="radio" name="option"
                                                                           value="2">
                                                                    <span class="span"></span>
                                                                    <div class="card-body p-0">

                                                                        <div
                                                                            class="flex-grow-1 bg-danger p-12 pb-40 card-rounded flex-grow-1 bgi-no-repeat">

                                                                            <p class="text-inverse-danger pt-10 pb-5 font-size-h3 font-weight-bolder line-height-lg">{{$question->optionB}}
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-3 col-xxl-3 col-lg-12">

                                                            <div class="card card-custom card-shadowless">
                                                                <label class="btn btn-primary">
                                                                    <input class="span" type="radio" name="option"
                                                                           value="3">
                                                                    <span class="span"></span>
                                                                    <div class="card-body p-0">

                                                                        <div
                                                                            class="flex-grow-1 bg-danger p-12 pb-40 card-rounded flex-grow-1 bgi-no-repeat">

                                                                            <p class="text-inverse-danger pt-10 pb-5 font-size-h3 font-weight-bolder line-height-lg">{{$question->optionC}}
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-xxl-3 col-lg-12">

                                                            <div class="card card-custom card-shadowless">
                                                                <label class="btn btn-primary">
                                                                    <input class="span" type="radio" name="option"
                                                                           value="4">
                                                                    <span class="span"></span>
                                                                    <div class="card-body p-0">

                                                                        <div
                                                                            class="flex-grow-1 bg-danger p-12 pb-40 card-rounded flex-grow-1 bgi-no-repeat">

                                                                            <p class="text-inverse-danger pt-10 pb-5 font-size-h3 font-weight-bolder line-height-lg">{{$question->optionD}}
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </label>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                        @endif

                                        @if($question->question_type == 2)
                                            @php
                                                $dataA = explode('{blank1}',$question->question);
                                                $dataB = explode('{blank2}',$question->question_other);
                                                $answerAoption = explode(',',$question->optionA);
                                                $answerBoption = explode(',',$question->optionB);
                                            @endphp
                                            <div class="mb-6">
                                                <div class="progress" style="height: 20px;">
                                                    <div class="progress-bar" role="progressbar" style="width: 25%;"
                                                         aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            <div class="row">


                                                <div class="card card-custom mb-6">
                                                    <div class="card-body rounded p-0 d-flex"
                                                         style="background-color:#DAF0FD;">
                                                        <div
                                                            class="d-flex flex-column flex-lg-row-auto w-auto p-10 p-md-20">
                                                            <h1 class="font-weight-bolder text-dark"><p
                                                                    style="width: 80vw;">
                                                                <p style="width: 80vw;">

                                                                    {{ count($dataA) > 0 ? $dataA[0] : '' }}
                                                                    <select style="width: 150px;font-size: 15px;"
                                                                            name="answerA"
                                                                            id="answerA" required>
                                                                        <option value="">Select Option</option>
                                                                        @if(!empty($answerAoption) && count($answerAoption) > 0)
                                                                            @foreach($answerAoption as $optA)
                                                                                <option
                                                                                    value="{{$optA}}">{{ $optA }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    {{ count($dataA) > 0 ? $dataA[1] : '' }}
                                                                    {{ count($dataB) > 0 ? $dataB[0] : '' }}
                                                                    <select style="width: 150px;font-size: 15px;"
                                                                            name="answerB"
                                                                            id="answerB" required>
                                                                        <option value="">Select Option</option>
                                                                        @if(!empty($answerBoption) && count($answerBoption) > 0)
                                                                            @foreach($answerBoption as $optB)
                                                                                <option
                                                                                    value="{{$optB}}">{{ $optB }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    {{ count($dataB) > 0 ? $dataB[1] : ''}}
                                                                </p>

                                                            </h1>
                                                        </div>
                                                        <div
                                                            class="d-none d-md-flex flex-row-fluid bgi-no-repeat bgi-position-y-center bgi-position-x-left bgi-size-cover"></div>
                                                    </div>
                                                    <div class="card-footer d-flex justify-content-between">
                                                        @endif
                                                        @if($question->question_id != $first_question_id)
                                                        @endif
                                                        @if($question->question_id == $last_question_id)
                                                            <button style="float:right!important" type="submit"
                                                                    class="btn btn-primary font-weight-bold font-size-h3 px-12 py-5">
                                                                Finish
                                                            </button>@else
                                                            <button style="float:right!important" type="submit"
                                                                    class="btn btn-light-success font-weight-bold font-size-h3 px-12 py-5">
                                                                Next
                                                            </button> @endif
                                                    </div>
                                                </div>
                                            </div>

                                    </form>
                                </div>

                                @if($questions->count()>1)
                                    @push('js')
                                        <script>
                                            $(function () {

                                                //console.log({{$question->question_id}});
                                                //console.log({{$last_question_id}});

                                                $('#frm{!!$question->question_id!!}').on('submit', function (e) {

                                                    $('#loader').show();
                                                    e.preventDefault();
                                                    var form = $(this);
                                                    var $formAction = form.attr('action');

                                                    var $userAnswer = $('input[name=option]:checked', $('#frm{{$question->question_id}}')).val();


                                                    $.post($formAction, $(this).serialize(), function (data) {

                                                        //
                                                        if (data.next_question_id > {{$last_question_id}}) {
                                                            {{--                                                            window.location.replace("{{ route('result', [$subject->question_id]) }}");--}}
                                                                window.location.href = '{{ route('index.home') }}';
                                                        }
                                                        $('#loader').hide();
                                                        $('#jake{{$question->question_id}}').hide();
                                                        //console.log(data.next_question_id);
                                                        $('#jake' + data.next_question_id + '').show();
                                                    });


                                                });
                                            });

                                        </script>
                                    @endpush
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <!--end::Charts Widget 2-->

                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
@endsection
