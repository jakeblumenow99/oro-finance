<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Quiz System</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('css/flipclock.css')}}">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->

        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->

            <ul class="nav navbar-nav">
                @can('admin')
                    <li>
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <span class="glyphicon glyphicon-home"></span> Home
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="{{ url('/home') }}" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">Modules <span class="caret"></span></a>
                        <ul class="dropdown-menu" id="manager-menu">
                            <li><span class="glyphicon glyphicon-cog"></span> <a href="{{route('difficulty.index')}}">Manager
                                    Difficulty Level</a></li>
                            <li><span class="glyphicon glyphicon-plus"></span> <a href="{{route('difficulty.create')}}">Add
                                    Difficulty Level</a></li>
                            <li role="separator" class="divider"></li>

                            <li><span class="glyphicon glyphicon-cog"></span> <a href="{{route('score.index')}}">Manager
                                    Scores</a></li>
                            <li><span class="glyphicon glyphicon-plus"></span> <a href="{{route('score.create')}}">Add
                                    Scores</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('user.index')}}">Users</a>
                    </li>

                    <li>
                        <a href="{{route('user.profile.update')}}">Profile Update</a>
                    </li>
                    {{--                                        <li>--}}
                    {{--                                            <a href="{{route('subjects.results')}}">Exams Results</a>--}}
                    {{--                                        </li>--}}

                @endcan

                @can('guest')
                <!--li>
                        <a href="{{route('start-exam',1)}}">Start Test</a>
                    </li-->
                    <li>
                        <a class="navbar-brand" href="{{ route('index.home') }}">
                            <span class="glyphicon glyphicon-home"></span> Home
                        </a>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">Choose exams <span class="caret"></span></a>
                        <ul class="dropdown-menu" id="manager-menu">


                            @foreach(App\Subject::all() as $cat)




                                @if($cat->hasQuestions())
                                    @php
                                        $takenQuiz = \App\Answer::where('user_id',auth()->user()->id)->where('subject_id',$cat->id)->groupBy('subject_id')->get();
                                        $checkUserClass = \App\ClassUser::where('class_id',$cat->classes->id)->where('user_id',auth()->user()->id)->get();
                                    @endphp
                                    @if( count($takenQuiz) > 0)
                                    @elseif(count($checkUserClass) > 0)
                                        <li>

                                            <a href="{{route('before-exam',$cat->id)}}">{{$cat->name}}</a>
                                        </li>
                                    @endif
                                @endif
                            @endforeach

                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('get.results') }}">Results</a>
                    </li>
                    <li>
                        <a href="{{ route('user.get.progression.level') }}">Progression Level</a>
                    </li>
                    <li>
                        <a href="{{route('user.profile.update')}}">Profile Update</a>
                    </li>

                @endcan
                @can('teacher')
                <!--li>
                        <a href="{{route('start-exam',1)}}">Start Test</a>
                    </li-->
                    <li>
                        <a class="navbar-brand" href="{{ route('index.home') }}">
                            <span class="glyphicon glyphicon-home"></span> Home
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="{{ url('/home') }}" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">Modules <span class="caret"></span></a>
                        <ul class="dropdown-menu" id="manager-menu">
                            <li><span class="glyphicon glyphicon-cog"></span> <a href="{{route('class.index')}}">Class
                                    Listing</a></li>
                            <li><span class="glyphicon glyphicon-plus"></span> <a href="{{route('class.create')}}">Add
                                    Class</a></li>
                            <li role="separator" class="divider"></li>
                            <li><span class="glyphicon glyphicon-cog"></span> <a href="{{route('category.index')}}">Manager
                                    Category</a></li>
                            <li><span class="glyphicon glyphicon-plus"></span> <a href="{{route('category.create')}}">Add
                                    Category</a></li>
                            <li role="separator" class="divider"></li>
                            <li><span class="glyphicon glyphicon-cog"></span> <a
                                    href="{{route('subject.index')}}">Manage Subjects</a></li>
                            <li><span class="glyphicon glyphicon-plus"></span> <a
                                    href="{{route('subject.new')}}">Add
                                    Subjects</a></li>
                            <li role="separator" class="divider"></li>
                            <li><span class="glyphicon glyphicon-cog"></span> <a
                                    href="{{route('questions.index')}}">Manage Question</a></li>
                            <li><span class="glyphicon glyphicon-plus"></span> <a
                                    href="{{route('questions.createnew')}}">Add
                                    Question</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('teacher.get.results') }}">Results</a>
                    </li>
                    <li>
                        <a href="{{ route('teacher.get.progression.level') }}">Students Progression Level</a>
                    </li>
                    <li>
                        <a href="{{route('user.profile.update')}}">Profile Update</a>
                    </li>

                @endcan
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <form action="{{ route('logout') }}" method="post" style=" padding-top: 7px;">
                                    @csrf

                                    <button class="" style="border-radius: 60px ;    cursor: pointer;"><i
                                            class="fa fa-btn fa-sign-out"></i> Logout
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @yield('content')
        </div>
    </div>
</div>


<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset('js/flipclock.min.js')}}"></script>
{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script>

    $('div.alert-success').delay(3000).slideUp(400);

    $(function () {
        $('a#btn-delete').on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var $a = this;

            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this category!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, delete it!',
                    closeOnConfirm: false
                },
                function () {
                    //console.log($($a).attr('href'));
                    document.location.href = $($a).attr('href');
                });
        });
    });

    $('#add-new-question').hide();
    $('#btn-add-new-question').on('click', function () {
        $('#add-new-question').slideDown();
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    });
    @yield('script_clock')
    @yield('script_form')
</script>
@stack('js')
</body>
</html>
