@extends('layouts.view')

@section('table_view')
    @if(!empty($allsubject) && count($allsubject) > 0)
        @php
            $obtainedMarks = 0;
            $totalPercentage = 0;
            $subjectCount =0;
             $userQuestion = 0;
             $percentage =0;
        @endphp
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>Quiz Name</th>
                    <th>UserName</th>
                    <th>Total Question</th>
                    <th>Correct Answers</th>
                    <th>Exam Date</th>
                </tr>
                </thead>


                @if(!empty($allsubject) && count($allsubject) > 0)
                    @foreach($allsubject as $subj)
                        @foreach($subj->classes->users as $user)
                            @if(!empty($subj->answers) && count($subj->answers) > 0)
                                @php
                                    $count =0;
                                    $userQuestion = 0;
                                    $examDate = null;
                                    $subjectCount++;
                                    $username = '';

                                    foreach($subj->answers as $ans){

                                        if($ans->user_id == $user->id){
                                            $userQuestion++;
                                            if($ans->question_type ==1 || $ans->question_type == 3) {
                                                    if($ans->user_answer == $ans->right_answer){
                                                         $count++;
                                                    }
                                            }
                                            if($ans->question_type ==2) {
                                                    if($ans->user_answerB == $ans->answerB){
                                                         $count =$count + 0.5;
                                                    }
                                                    if($ans->user_answerA == $ans->answerA){
                                                          $count =$count + 0.5;

                                                    }

                                            }


                                        }
                                        $totalQuestions =$userQuestion;
                                        $examDate = $ans->created_at;


                                    }

                                    if(!count($subj->answers) > 0){
                                        $percentage = ($count/$totalQuestions)*100;
                                        $obtainedMarks = $obtainedMarks + $percentage;
                                    }


                                @endphp

                                <tr>
                                    <td>{{$subj->name}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$totalQuestions}}</td>
                                    <td>{{$count}}</td>
                                    <td>{{$examDate}}</td>
                                </tr>
                            @endif
                        @endforeach

                    @endforeach
                @endif
            </table>
        </div>
    @endif
@endsection


@section('pagination')

@endsection
