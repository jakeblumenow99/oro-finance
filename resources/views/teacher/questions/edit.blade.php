@extends('layouts.view')

@section('table_view')
    @include('errors.list')
    <form action="{{ route('questions.updatequestion',[$question->question_id]) }}" method="post"
          class="form-horizontal" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label class="col-md-2 control-label" for="users">Question Type</label>
            <div class="col-md-5">
                <select class="form-control" name="questiontype" id="questiontype">
                    <option value=""> Select Question Type</option>
                    <option value="1" {{ $question->question_type == 1 ? 'selected' : '' }}> MCQ</option>
                    <option value="2" {{ $question->question_type == 2 ? 'selected' : '' }}> Fill in the blank</option>
                    <option value="3" {{ $question->question_type == 3 ? 'selected' : '' }}> Graph Question</option>
                </select>
            </div>
        </div>
        <div id="wholequestion">
            <div class="form-group">
                <label class="col-md-2 control-label" for="question">Question</label>
                <div class="col-md-10">
                    <input type="text" name="question" id="question" class="form-control"
                           value="{{$question->question}}">
                </div>
            </div>
            <div class="form-group" id="fillOther">
                <label class="col-md-2 control-label" for="question_other"
                       style="display: {{$question->question_type == 2 ? '' : 'none'}}">Question Other</label>
                <div class="col-md-10">
                    <input type="text" name="question_other" id="question_other" class="form-control"
                           value="{{ $question->question_other }}">
                </div>
            </div>
            <div id="fitb" style="display: {{ $question->question_type == 2 ? '' : 'none' }};">
                <div class="form-group">
                    <label class="col-md-offset-2 col-md-10">Note : For multiple blanks add options separated by
                        comma</label>
                </div>
            </div>
            <div id="graph" style="display: {{ $question->question_type == 3 ? '' : 'none' }};">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="question">Upload Graph</label>
                    <div class="col-md-10">
                        <input type="file" name="uploadfile" id="uploadfile" class="form-control">
                        <img src="{{asset('storage/images/'.$question->graph_image) }}" style="width: 20%;">
                    </div>

                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="question">Option A</label>
                <div class="col-md-10">
                    <input type="text" name="optiona" id="optiona" class="form-control" value="{{$question->optionA}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="question">Option B</label>
                <div class="col-md-10">
                    <input type="text" name="optionb" id="optionb" class="form-control" value="{{$question->optionB}}">
                </div>
            </div>
            <div class="forMcqs"
                 style="display: {{$question->question_type == 1 || $question->question_type == 3   ? '' : 'none'}}">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="question">Option C</label>
                    <div class="col-md-10">
                        <input type="text" name="optionc" id="optionc" class="form-control"
                               value="{{$question->optionC}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="question">Option D</label>
                    <div class="col-md-10">
                        <input type="text" name="optiond" id="optiond" class="form-control"
                               value="{{$question->optionD}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="answer">Answer</label>
                    <div class="col-md-10">
                        <select class="form-control" name="answer" id="questiontype">
                            <option value=""> Select right answer</option>
                            <option value="1" {{ $question->Answer == 1 ? 'selected' : '' }}> Option A</option>
                            <option value="2" {{ $question->Answer == 2 ? 'selected' : '' }}> Option B</option>
                            <option value="3" {{ $question->Answer == 3 ? 'selected' : '' }}> Option C</option>
                            <option value="4" {{ $question->Answer == 4 ? 'selected' : '' }}> Option D</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="forFillInTheBlanks" style="display: {{$question->question_type == 2  ? '' : 'none'}}">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="answerA">Type Right Answer A</label>
                    <div class="col-md-10">
                        <input type="text" name="answerA" id="answerA" class="form-control"
                               value="{{ $question->answerA}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="answerB">Type Right Answer B</label>
                    <div class="col-md-10">
                        <input type="text" name="answerB" id="answerB" class="form-control"
                               value="{{ $question->answerB}}">
                    </div>
                </div>

            </div>


            <div class="form-group">
                <label class="col-md-2 control-label" for="difficulty_level_id">Difficulty Levels</label>
                <div class="col-md-7">
                    <select class="form-control" name="difficulty_level_id" id="difficulty_level_id">
                        <option value="">Select Difficulty levels</option>
                        @if($difficultyLevels)
                            @foreach($difficultyLevels as $difficultyLevel)
                                <option
                                    value="{{ $difficultyLevel->id }}" {{ (isset($question) && !is_null($question->difficulty_level_id) && $question->difficulty_level_id == $difficultyLevel->id) ? 'selected' : '' }}> {{ ucfirst($difficultyLevel->title) }}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="difficulty_level_id">Categories</label>
                <div class="col-md-7">
                    <select class="form-control" name="categories[]" id="categories" multiple="multiple">
                        {{--                    <option value="">Select Category</option>--}}
                        @if($categories)
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}"

                                        @if(!empty($categoriesData) && count($categoriesData) > 0)
                                        @foreach($categoriesData as $catData)
                                        @if($catData->id == $category->id)
                                        selected
                                    @endif
                                    @endforeach
                                    @endif
                                > {{ ucfirst($category->name) }}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Update Question</button>
            </div>
        </div>

    </form>



@endsection
@push('js')
    <script>
        $('#categories').select2({
            placeholder: "Select Category",
            maximumInputLength: 20 // only allow terms up to 20 characters long
        });
        $('#questiontype').select2({
            placeholder: "Select Students",
            maximumInputLength: 20 // only allow terms up to 20 characters long
        });
        $("#questiontype").on('change', function () {
            if ($("#questiontype").val() == 1) {
                $("#wholequestion").show();
                $(".forMcqs").show();
                $("#graph").hide();
                $("#fillOther").hide();
                $("#fitb").hide();
                $('.forFillInTheBlanks').hide();
            } else if ($("#questiontype").val() == 2) {
                $("#wholequestion").show();
                $(".forMcqs").hide();
                $("#graph").hide();
                $("#fillOther").show();
                $("#fitb").show();
                $('.forFillInTheBlanks').show();
            } else if ($("#questiontype").val() == 3) {
                $("#wholequestion").show();
                $("#fillOther").hide();
                $(".forMcqs").show();
                $("#graph").show();
                $("#fitb").hide();
                $('.forFillInTheBlanks').hide();
            }
        });
        $('#status').on('click', function () {
            var value = $(this).val();
            if (value === "0") {
                $('#status').val(1);
            } else {
                $('#status').val(0);
            }
        });

    </script>
@endpush

