@extends('layouts.view')

@section('table_view')
    @include('errors.list')
    <form action="{{ route('questions.storequestion') }}" method="post" class="form-horizontal"
          enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label class="col-md-2 control-label" for="users">Question Type</label>
            <div class="col-md-5">
                <select class="form-control" name="questiontype" id="questiontype">
                    <option value=""> Select Question Type</option>
                    <option value="1"> MCQ</option>
                    <option value="2"> Fill in the blank</option>
                    <option value="3"> Graph Question</option>
                </select>
            </div>
        </div>
        <div id="wholequestion" style="display: none;">
            <div class="form-group">
                <label class="col-md-2 control-label" for="question">Question</label>
                <div class="col-md-10">
                    <input type="text" name="question" id="question" class="form-control">
                </div>
            </div>
            <div class="form-group" id="fillOther">
                <label class="col-md-2 control-label" for="question_other">Question Other</label>
                <div class="col-md-10">
                    <input type="text" name="question_other" id="question_other" class="form-control">
                </div>
            </div>
            <div id="fitb" style="display: none;">
                <div class="form-group">
                    <label class="col-md-offset-2 col-md-10">Note : For multiple blanks add options separated by
                        comma</label>
                </div>
            </div>
            <div id="graph" style="display: none;">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="question">Upload Graph</label>
                    <div class="col-md-10">
                        <input type="file" name="uploadfile" id="uploadfile" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="question">Option A</label>
                <div class="col-md-10">
                    <input type="text" name="optiona" id="optiona" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="question">Option B</label>
                <div class="col-md-10">
                    <input type="text" name="optionb" id="optionb" class="form-control">
                </div>
            </div>

            <div class="forMcqs">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="question">Option C</label>
                    <div class="col-md-10">
                        <input type="text" name="optionc" id="optionc" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="question">Option D</label>
                    <div class="col-md-10">
                        <input type="text" name="optiond" id="optiond" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="answer">Answer</label>
                    <div class="col-md-10">
                        <select class="form-control" name="answer" id="questiontype">
                            <option value=""> Select right answer</option>
                            <option value="1"> Option A</option>
                            <option value="2"> Option B</option>
                            <option value="3"> Option C</option>
                            <option value="4"> Option D</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="forFillInTheBlanks">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="answerA">Type Right Answer A</label>
                    <div class="col-md-10">
                        <input type="text" name="answerA" id="answerA" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="answerB">Type Right Answer B</label>
                    <div class="col-md-10">
                        <input type="text" name="answerB" id="answerB" class="form-control">
                    </div>
                </div>

            </div>


            <div class="form-group">
                <label class="col-md-2 control-label" for="difficulty_level_id">Difficulty Levels</label>
                <div class="col-md-7">
                    <select class="form-control" name="difficulty_level_id" id="difficulty_level_id">
                        <option value="">Select Difficulty levels</option>
                        @if($difficultyLevels)
                            @foreach($difficultyLevels as $difficultyLevel)
                                <option
                                    value="{{ $difficultyLevel->id }}" {{ (isset($question) && !is_null($question->difficultyLevel) && $question->difficultyLevel->id == $difficultyLevel->id) ? 'selected' : '' }}> {{ ucfirst($difficultyLevel->title) }}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label" for="difficulty_level_id">Categories</label>
                <div class="col-md-7">
                    <select class="form-control" name="categories[]" id="categories" multiple="multiple">
                        {{--                    <option value="">Select Category</option>--}}
                        @if($categories)
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}"> {{ ucfirst($category->name) }}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Create Question</button>
            </div>
        </div>

    </form>



@endsection
@push('js')
    <script>
        $('#categories').select2({
            placeholder: "Select Category",
            maximumInputLength: 20 // only allow terms up to 20 characters long
        });
        $('#questiontype').select2({
            placeholder: "Select Students",
            maximumInputLength: 20 // only allow terms up to 20 characters long
        });
        $("#questiontype").on('change', function () {
            if ($("#questiontype").val() == 1) {
                $("#wholequestion").show();
                $(".forMcqs").show();
                $("#graph").hide();
                $("#fillOther").hide();
                $("#fitb").hide();
                $('.forFillInTheBlanks').hide();
            } else if ($("#questiontype").val() == 2) {
                $("#wholequestion").show();
                $(".forMcqs").hide();
                $("#graph").hide();
                $("#fillOther").show();
                $("#fitb").show();
                $('.forFillInTheBlanks').show();
            } else if ($("#questiontype").val() == 3) {
                $("#wholequestion").show();
                $("#fillOther").hide();
                $(".forMcqs").show();
                $("#graph").show();
                $("#fitb").hide();
                $('.forFillInTheBlanks').hide();
            }
        });
        $('#status').on('click', function () {
            var value = $(this).val();
            if (value === "0") {
                $('#status').val(1);
            } else {
                $('#status').val(0);
            }
        });

    </script>
@endpush

