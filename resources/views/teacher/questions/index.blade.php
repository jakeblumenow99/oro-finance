@extends('layouts.view')
@section('table_view')

    <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th>Question</th>
                <th>Question Type</th>
                <th>Categories</th>
                <th style="width: 40%;">Image</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>

            @foreach($questions as $question)
                @php
                    $categories = \App\CatQuestion::where('question_id',$question->question_id)->pluck('category_id')->toArray();
                    if(!empty($categories) && count($categories) > 0){
                        $categoriesData = \App\Category::whereIn('id',$categories)->get();
                    }else{
                        $categoriesData =[];
                    }
                @endphp
                <tr>
                    <td>{{$question->question}} {{$question->question_other  }}</td>
                    <td>
                        @if($question->question_type == 1)
                            MCQ
                        @elseif($question->question_type == 2)
                            Fill in the blanks
                        @else
                            Graph Question
                        @endif
                    </td>
                    <td>
                        @if(!empty($categoriesData) && count($categoriesData) > 0)
                            <ul>
                                @foreach($categoriesData as $cat)
                                    <li>{{ $cat->name }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </td>
                    <td>
                        @if(!is_null($question->graph_image))
                            <img src="{{asset('storage/images/'.$question->graph_image) }}" style="width: 20%;">
                        @else
                            Not Found
                        @endif

                    </td>
                    <td>
                        <h4>
                            @if($question->status == 'Active')
                                <span class="label label-success">Active</span>
                            @else
                                <span class="label label-warning">Inactivate</span>
                            @endif
                        </h4>
                    </td>
                    <td>
                        <a class="btn btn-warning"
                           href="{{route('questions.edit', [$question->question_id])}}">Edit</a>
                        <form method="post" id="delete_{{ $question->id }}" style="display: inline">
                            @method('delete')
                            @csrf
                            <a class="btn btn-danger" id="btn-delete-category"
                               href="javascript:void(0)"
                               onclick="document.getElementById('delete_<?=$question->id?>').submit();">
                                Delete
                            </a>
                        </form>
                    </td>

                </tr>
            @endforeach
        </table>
    </div>
@endsection
