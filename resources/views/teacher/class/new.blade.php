@extends('layouts.view')

@section('table_view')
    @include('errors.list')
    <form action="{{ route('class.store') }}" method="post" class="form-horizontal">
        @csrf
        <div class="form-group">
            <label class="col-md-2 control-label" for="name">Name of Class</label>
            <div class="col-md-10">
                <input type="text" name="name" id="name" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="slug">Slug</label>
            <div class="col-md-10">
                <input type="text" name="slug" id="slug" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="description">Description</label>
            <div class="col-md-10">
                <textarea name="description" id="description" class="form-control" rows="5" cols="5"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="users">Students</label>
            <div class="col-md-5">
                <select class="form-control" name="users[]" id="users"  multiple="multiple">
                    <option value="">Select Student</option>
                    @if(!empty($users) && count($users) > 0)
                        @foreach($users as $user)
                            <option
                                value="{{ $user->id }}"> {{ ucfirst($user->name) }}</option>
                        @endforeach
                    @endif

                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="status">Active</label>
            <div class="col-md-10">
                <input type="checkbox" name="status" id="status" value="0">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Create Class</button>

            </div>
        </div>

    </form>



@endsection
@push('js')
    <script>
        $('#users').select2({
            placeholder: "Select Students",
            maximumInputLength: 20 // only allow terms up to 20 characters long
        });
        $('#status').on('click', function () {
            var value = $(this).val();
            if (value === "0") {
                $('#status').val(1);
            } else {
                $('#status').val(0);
            }
        });

    </script>
@endpush

