@extends('layouts.view')

@section('table_view')
    @include('errors.list')


    <form action="{{ route('class.update',$class->id) }}" method="post" class="form-horizontal">
        @csrf
        @method('patch')
        <div class="form-group">
            <label class="col-md-2 control-label" for="name">Name of Class</label>
            <div class="col-md-10">
                <input type="text" name="name" id="name" value="{{ $class->name }}" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="slug">Slug</label>
            <div class="col-md-10">
                <input type="text" name="slug" id="slug" value="{{ $class->slug }}" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="description">Description</label>
            <div class="col-md-10">
                <textarea name="description" id="description" class="form-control" rows="5" cols="5">{{ $class->description }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="users">Students</label>
            <div class="col-md-5">
                <select class="form-control" name="users[]" id="users" multiple="multiple">
                    <option value="">Select Student</option>
                    @if(!empty($users) && count($users) > 0)
                        @foreach($users as $user)
                            <option
                                value="{{ $user->id }}"

                                @if(!empty($class->users) && $class->users()->count() > 0)
                                    @foreach($class->users as $userClass)
                                        @if($user->id == $userClass->id)
                                             selected
                                        @endif
                                    @endforeach
                                @endif
                            > {{ ucfirst($user->name) }}</option>
                        @endforeach
                    @endif

                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="status">Active</label>
            <div class="col-md-10">
                <input type="checkbox" name="status" id="status"
                       value="{{$class->status}}" {{ ($class->status == 1) ? 'checked' : '' }}>
            </div>
        </div>

        <div class="form-group" style="    display: inline-flex;">

            <div class="col-sm-offset-2 col-sm-6">
                <button type="submit" class="btn btn-primary">Update Class</button>

            </div>
            <div class="col-sm-offset-2 col-sm-6">
                <a class="btn btn-primary" href="{{route('class.index')}}">Class List</a>

            </div>

        </div>

    </form>
@endsection
@push('js')
    <script>
        $('#users').select2({
            placeholder: "Select Students",
            maximumInputLength: 20 // only allow terms up to 20 characters long
        });
        $('#status').on('click', function () {
            var value = $(this).val();
            if (value === "0") {
                $('#status').val(1);
            } else {
                $('#status').val(0);
            }
        });

    </script>
@endpush

