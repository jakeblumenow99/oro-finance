@extends('layouts.view')

@section('table_view')
    @if(!$classes->isEmpty())
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                        <th>Class name</th>
                        <th>Slug</th>
                        <th>Description</th>
                        <th>Students in Class</th>
                        <th>Status</th>
                        <th>Action</th>
                </tr>
                </thead>
                @foreach($classes as $class)
                    <tr>
                        <td>{{$class->name}}</td>
                        <td>{{$class->slug}}</td>
                        <td>{{$class->description}}</td>
                        <td>
                            <ul>
                                @if(!empty($class->users) && $class->users()->count() > 0)
                                    @foreach($class->users as $user)
                                        <li>{{ ucfirst($user->name) }}</li>
                                    @endforeach
                                @endif

                            </ul>
                        </td>
                        <td>
                            <h4>
                                @if($class->status == 1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-warning">Inactivate</span>
                                @endif
                            </h4>
                        </td>
                        <td>
                            <a class="btn btn-warning" href="{{route('class.edit', [$class->id])}}">Edit</a>
                            <form method="post" action="{{ route('class.destroy', $class->id) }}"
                                  id="delete_{{ $class->id }}" style="display: inline">
                                @method('delete')
                                @csrf
                                <a class="btn btn-danger" id="btn-delete-category"
                                   href="javascript:void(0)"
                                   onclick="document.getElementById('delete_<?=$class->id?>').submit();">
                                    Delete
                                </a>
                            </form>
                        </td>

                    </tr>
                @endforeach

            </table>
        </div>
    @endif
@endsection

@section('pagination')
    {{$classes->links()}}
@endsection
