@extends('layouts.view')

@section('table_view')
    @if(!empty($usersArray) && count($usersArray) > 0)
        <div class="table-responsive">
            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Level</th>
                </tr>
                </thead>


                @if(!empty($usersArray) && count($usersArray) > 0)

                    @foreach($usersArray as $key=>$user)
                        @php
                            $level='';
                              $points = \App\Points::all();
                              foreach ($points as $point){
                                  if($point->points <= $progessionLevel[$key]){
                                      $level = $point->title;
                                      break;
                                  }else{
                                      $level = 'No Level';
                                  }
                              }

                        @endphp

                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$level}}</td>
                        </tr>

                    @endforeach
                @endif
            </table>

        </div>
    @endif
@endsection


@section('pagination')

@endsection
