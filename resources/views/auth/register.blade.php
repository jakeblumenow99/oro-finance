@extends('guestLayout.app')

@section('content')
    <!--begin::Register-->
    <div class="login login-4 wizard d-flex flex-column flex-lg-row flex-column-fluid">
        <!--begin::Content-->
        <div
            class="login-container order-2 order-lg-1 d-flex flex-center flex-row-fluid px-7 pt-lg-0 pb-lg-0 pt-4 pb-6 bg-white">
            <!--begin::Wrapper-->
            <div class="login-content d-flex flex-column pt-lg-0 pt-12">
                <!--begin::Logo-->
                <a href="#" class="login-logo pb-xl-7 pb-7">
                    <img src="https://orofinance.com/app/assets/media/logos/logo-letter-1.png" class="max-h-70px" style="margin-top: 50px;" alt=""/>
                </a>
                <!--end::Logo-->
                <!--begin::Signin-->
                <div class="login-form">
                    <!--begin::Form-->
                    <form class="form" method="POST" action="{{ url('/register') }}">
                    {!! csrf_field() !!}
                    <!--begin::Title-->
                        <div class="pb-5 pb-lg-15">
{{--                            <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Sign UP</h3>--}}
                            <div class="text-muted font-weight-bold font-size-h4">Already have an account?
                                <a href="{{ route('login') }}"
                                   class="text-primary font-weight-bolder">Sign In</a></div>
                        </div>
                        <!--begin::Title-->

                        <!--begin::Form group-->
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="font-size-h6 font-weight-bolder text-dark" for="name">Your Name</label>
                            <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0"
                                   value="{{ old('name') }}" id="name"
                                   type="text" name="name" autocomplete="off"/>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <!--end::Form group-->

                        <!--begin::Form group-->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="font-size-h6 font-weight-bolder text-dark" for="email">Your @aston.ac.uk email (only enter your username)</label>
{{--                            <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0"--}}
{{--                                   value="{{ old('email') }}"--}}
{{--                                   type="email" name="email" autocomplete="off" id="email"/>--}}
                            <div class="input-group">
                                <input  type="text" name="email" autocomplete="off" id="email" class="form-control py-7 px-6 h-auto" placeholder="Username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <span class="input-group-text">@aston.ac.uk</span>
                                </div>
                            </div>

                        </div>
                        <!--end::Form group-->
                        <!--begin::Form group-->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="font-size-h6 font-weight-bolder text-dark pt-5">Your Password</label>
                            <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0"
                                   type="password" name="password" autocomplete="off" id="password"/>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <!--end::Form group-->

                        <!--begin::Form group-->
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="font-size-h6 font-weight-bolder text-dark pt-5" for="password_confirmation">Your Password Confirmation</label>
                            <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0"
                                   type="password" name="password_confirmation" autocomplete="off" id="password_confirmation"/>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <!--end::Form group-->
                        <div class="pb-lg-0 pb-5" style="color: white;">
                            <button type="submit" id="kt_login_singin_form_submit_button"
                                    class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Sign UP
                            </button>
                        </div>
                        <!--begin::Form group-->
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}" style="visibility: hidden;">
                            <label class="font-size-h6 font-weight-bolder text-dark pt-5" for="type">Role</label>


                            <select class="form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0" name="type" id="type"  autocomplete="off">
{{--                                <option--}}
{{--                                    value="teacher" {{ (old('type') == 'teacher') ? 'selected' :'' }}>--}}
{{--                                    Teacher--}}
{{--                                </option>--}}
                                <option value="user" {{ (old('type') == 'user') ? 'selected' :'' }}>User
                                </option>
                            </select>
                            @if ($errors->has('type'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <!--end::Form group-->

                        <!--begin::Action-->

                        <!--end::Action-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Signin-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--begin::Content-->
        <!--begin::Aside-->
        <div class="login-aside order-1 order-lg-2 bgi-no-repeat bgi-position-x-right">
            <div class="login-conteiner bgi-no-repeat bgi-position-x-right bgi-position-y-bottom"
                 style="background-image: url({{ asset('assets/media/svg/illustrations/login-visual-4.svg') }});">
                <!--begin::Aside title-->
                <h3 class="pt-lg-40 pl-lg-20 pb-lg-0 pl-10 py-20 m-0 d-flex justify-content-lg-start font-weight-boldest display5 display1-lg text-white">
                    Oro<br>Finance.
                </h3>
                <!--end::Aside title-->
            </div>
        </div>
        <!--end::Aside-->
    </div>
    <!--end::Register-->
@endsection
