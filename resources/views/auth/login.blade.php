@extends('guestLayout.app')

@section('content')
    <!--begin::Login-->
    <div class="login login-4 wizard d-flex flex-column flex-lg-row flex-column-fluid">
        <!--begin::Content-->
        <div
            class="login-container order-2 order-lg-1 d-flex flex-center flex-row-fluid px-7 pt-lg-0 pb-lg-0 pt-4 pb-6 bg-white">
            <!--begin::Wrapper-->
            <div class="login-content d-flex flex-column pt-lg-0 pt-12">
                <!--begin::Logo-->
                <a href="#" class="login-logo pb-xl-7 pb-7">
                    <img src="https://orofinance.com/app/assets/media/logos/logo-letter-1.png" class="max-h-70px"
                         alt=""/>
                </a>
                <!--end::Logo-->
                <!--begin::Signin-->
                <div class="login-form">
                    <!--begin::Form-->
                    <form oninput="email.value = txtUser.value + '@aston.ac.uk'" class="form" method="POST" action="{{ url('/login') }}">
                    {!! csrf_field() !!}
                    <!--begin::Title-->
                        <div class="pb-5 pb-lg-15">
                            <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">Sign In</h3>
                            <div class="text-muted font-weight-bold font-size-h4">New Here?
                                <a href="{{ route('register') }}"
                                   class="text-primary font-weight-bolder">Create Account</a></div>
                        </div>
                        <!--begin::Title-->
                        <!--begin::Form group-->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
{{--                            <div class="input-group input-group-solid">--}}
{{--                                <input type="text" class="form-control form-control-solid" placeholder="Username" value="loop">--}}
{{--                                <div class="input-group-append">--}}
{{--                                    <span class="input-group-text">.com</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <label class="font-size-h6 font-weight-bolder text-dark">Your Email</label>
                            {{--                            <div class="input-group">--}}
                            {{--                                <input style="font-size: 1.5rem" type="text" name="email" autocomplete="off" id="email" class="form-control py-7 px-6 h-auto" placeholder="Username" aria-describedby="basic-addon2">--}}
                            {{--                                <div class="input-group-append">--}}
                            {{--                                    <span style="font-size: 1.5rem" class="input-group-text">@aston.ac.uk</span>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        <div class="input-group input-group-solid">
                            <input style="border-top-right-radius: 0px!important;border-bottom-right-radius: 0px!important;" class="form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0"
                                   value="{{ old('email') }}" placeholder="180192791"
                                   type="text" name="txtUser" autocomplete="off"/>
                            <div class="input-group-append">
                                <span class="input-group-text">@aston.ac.uk</span>
                                <input name="email" type="hidden">
                            </div>
                        </div>
{{--                            @if ($errors->has('email'))--}}
{{--                                <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('email') }}</strong>--}}
{{--                                    </span>--}}
{{--                            @endif--}}
                        </div>
                        <!--end::Form group-->
                        <!--begin::Form group-->
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="d-flex justify-content-between mt-n5">
                                <label class="font-size-h6 font-weight-bolder text-dark pt-5">Your Password</label>
                                <a href="{{ url('/password/reset') }}"
                                   class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5">Forgot
                                    Password ?</a>
                            </div>
                            <input style=""
                                   class="form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0"
                                   type="password" name="password" autocomplete="off"/>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <!--end::Form group-->
                        <!--begin::Action-->
                        <div class="pb-lg-0 pb-5" style="color: white;">
                            <button type="submit" id="kt_login_singin_form_submit_button"
                                    class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">Sign In
                            </button>
                        </div>
                        <!--end::Action-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Signin-->
            </div>
            <!--end::Wrapper-->
        </div>
        <!--begin::Content-->
        <!--begin::Aside-->
        <div class="login-aside order-1 order-lg-2 bgi-no-repeat bgi-position-x-right">
            <div class="login-conteiner bgi-no-repeat bgi-position-x-right bgi-position-y-bottom"
                 style="background-image: url({{ asset('assets/media/svg/illustrations/login-visual-4.svg') }});">
                <!--begin::Aside title-->
                <h3 class="pt-lg-40 pl-lg-20 pb-lg-0 pl-10 py-20 m-0 d-flex justify-content-lg-start font-weight-boldest display5 display1-lg text-white">
                    Oro<br> Finance.

                </h3>
                <!--end::Aside title-->
            </div>
        </div>
        <!--end::Aside-->
    </div>
    <!--end::Login-->
@endsection
