<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['name', 'status', 'duration', 'category_id', 'class_id', 'question_limit'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }


    public function classes()
    {
        return $this->belongsTo(Classes::class, 'class_id', 'id');
    }

    public function questions()
    {
        return $this->hasMany(Questions_tab::class, 'question_id', 'id');
    }

    public function hasQuestions()
    {
        if ($this->questions()->get()->count()) {
            return true;
        } else {
            return false;
        }
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function isExamined()
    {

        $status = false;
        if ($this->answers()->get()->count()) {
            foreach ($this->answers()->get() as $check) {
                if ($check->user_id == auth()->user()->id) {
                    $status = true;
                } else {
                    $status = false;
                }
            }
            return $status;
        }
        return true;
    }
}
