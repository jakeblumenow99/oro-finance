<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DifficultyLevel extends Model
{
    protected $fillable = ['title', 'points'];

    public function questions()
    {
        return $this->hasMany(Question::class, 'difficulty_level_id', 'id');
    }
}
