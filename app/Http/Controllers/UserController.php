<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate(5);
        $title = 'Users Listing';
        return view('user.index', compact('users', 'title'));
    }

    public function profileEdit()
    {
        $title = 'Profile Update';

        $user = Auth::user();
        if ($user->roles[0]->name == 'guest') {
            return view('user/profiles/index', compact('title'));
        } else {
            return view('profiles/index', compact('title'));
        }

    }

    public function profileUpdate(Request $request)
    {
        $user = Auth::user();

        if (!is_null($request->email)) {
            $user->email = $request->email;
            $user->save();
        }
        if (!is_null($request->username)) {
            $user->name = $request->username;
            $user->save();
        }
        if (!(Hash::check($request->get('current-password'), auth()->user()->password))) {
            // The passwords matches
            return redirect()->back()->with(
                [
                    'flash_status' => 'error',
                    'flash_message' => 'Your current password does not matches with the password you provided. Please try again.'
                ]);
        }

        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same

            return redirect()->back()->with(
                [
                    'flash_status' => 'error',
                    'flash_message' => 'New Password cannot be same as your current password. Please choose a different password..'
                ]);
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6',
        ]);

        if (strcmp($request->get('new-password-confirm'), $request->get('new-password')) == 0) {
            //Change Password

            $user->password = bcrypt($request->get('new-password'));


            $user->save();

            return redirect()->route('user.profile.edit')
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Password Changed successfully.'
                ]);

        } else {
            return redirect()->back()
                ->with([
                    'flash_status' => 'error',
                    'flash_message' => 'New Password must be same as your confirm password.'
                ]);
        }
    }
}
