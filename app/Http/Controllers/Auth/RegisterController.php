<?php

namespace App\Http\Controllers\Auth;

use App\Classes;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'type' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'] . '@aston.ac.uk';
        $user->password = Hash::make($data['password']);
        $user->save();
        if ($data['type'] == 'teacher') {
            \Illuminate\Support\Facades\DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => 3
            ]);

        } else {
            \Illuminate\Support\Facades\DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => 2
            ]);

            $classes = Classes::where('name', 'BS1196')->get();
            if (!empty($classes) && count($classes) > 0) {
                foreach ($classes as $class) {
                    $class->users()->attach($user);
                }

            }
        }

        return $user;
    }

    protected function registered(Request $request, $user)
    {
        if ($user->roles[0]->name == 'guest') {
            return redirect()->route('student.dashboard');
        } else {
            return redirect()->to($this->redirectTo);
        }
    }
}
