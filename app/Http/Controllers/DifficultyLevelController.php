<?php

namespace App\Http\Controllers;

use App\Classes;
use App\DifficultyLevel;
use App\User;
use Illuminate\Http\Request;

class DifficultyLevelController extends Controller
{
    public function index()
    {
        $difficultys = DifficultyLevel::orderBy('id', 'DESC')->paginate(5);
        $title = 'Difficulty Level Listing';
        return view('difficulty.index', compact('difficultys', 'title'));
    }

    public function create()
    {
        $title = "Create New Difficulty Level";

        return view('difficulty.new',compact('title'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'points' => 'required',
        ], [
            'title.required' => 'Title is required.',
            'points.required' => 'Points is required.',
        ]);

        $difficulty = new DifficultyLevel();
        $difficulty->title = $request->title;
        $difficulty->points = $request->points;
        $difficulty->save();
        session()->flash('flash_mess', 'Difficulty Level created successfully');
        return redirect()->route('difficulty.index');

    }

    public function edit($id)
    {
        $difficulty = DifficultyLevel::find($id);
        $title = "Edit Difficulty '{$difficulty->title}'";
        return view('difficulty.edit', compact('difficulty','title'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'points' => 'required',
        ], [
            'title.required' => 'Title is required.',
            'points.required' => 'Points is required.',
        ]);

        $difficulty = DifficultyLevel::find($id);
        $difficulty->title = $request->title;
        $difficulty->points = $request->points;
        $difficulty->save();
        session()->flash('flash_mess', 'Difficulty Level Updated successfully');
        return redirect()->route('difficulty.index');


    }

    public function destroy($id)
    {

        $difficulty = DifficultyLevel::findOrFail($id);
        $difficulty->delete();

        return redirect()->route('difficulty.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Difficulty Level has been deleted'
            ]);
    }
}
