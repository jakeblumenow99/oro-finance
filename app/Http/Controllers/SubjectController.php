<?php

namespace App\Http\Controllers;

use App\Classes;
use App\ClassUser;
use App\DifficultyLevel;
use App\Questions_tab;
use App\Progression;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Subject;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\Answer;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\SubjectRequest;
use App\Http\Requests\QuestionRequest;
use App\Question;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Type\Collection;

class SubjectController extends Controller
{
    public function index()
    {
        $subjects = Subject::paginate(5);
        $title = "Subjects Listing";
//        $categories = Category::lists('name', 'id');
        return view('subject.index', compact('subjects', 'title'));
    }

    public function getNew()
    {

        $title = "Create New Subject";

        $categories = Category::all();
        $classes = Classes::all();
        return view('subject.new', compact('title', 'categories', 'classes'));
    }

    public function postNewSubject(SubjectRequest $req)
    {
//       dd($req->all());

        foreach ($req->input('categories') as $cat) {
            $subject = new Subject($req->all());

            $subject->name = $req->name;
            $subject->duration = $req->duration;
            $subject->question_limit = $req->question_limit;
            $subject->status = $req->status;
            $subject->category_id = $cat;
            $subject->class_id = $req->classes;
            $subject->save();


//
//            dd($subject);
//            foreach ($req->input('classes') as $cla){
//
//
//
//                $classss = Classes::find((int)$cla);
//
//                $category = Category::find((int)$cat);
//                $category->subjects()->save($subject);
//                $classss->subjects()->save($subject);
//            }

        }


        session()->flash('flash_mess', 'Subject was created completely');
        return redirect(route('subject.index'));
    }

    public function getEdit($id)
    {

        $subject = Subject::findOrFail($id);
        $title = "Edit subject '{$subject->name}'";
        $selectedCategoryId = $subject->category()->pluck('id')[0];
//        dd($selectedCategoryId);
        $categories = Category::all();
        return view('subject.edit', compact('subject', 'title', 'categories', 'selectedCategoryId'));
    }

    public function patchEdit($id, SubjectRequest $req)
    {

        if ($req->get('status') == null)
            $req['status'] = 0;

        $subject = Subject::findOrFail($id);
        $subject->category_id = $req->input('categories');
        $subject->update($req->all());
        session()->flash('flash_mess', 'Subject was changed completely');
        return redirect(route('subject.question.edit', $subject->id));
    }

    public function getDelete($id)
    {

        $subject = Subject::findOrFail($id);
        Subject::destroy($id);
        session()->flash('flash_mess', 'Subject  was deleted');
        return redirect(route('subject.index'));
    }

    public function getQuestions($id)
    {

        $subject = Subject::findOrFail($id);

        $title = "Manage questions";
        $answer = ['1' => 1, '2' => 2, '3' => 3, '4' => 4];
        $difficultyLevels = DifficultyLevel::all();

        $questions_tab = Questions_tab::all();
        $qs = \App\CatQuestion::where('category_id', $subject->category_id)->pluck('question_id')->toArray();
        if (!empty($qs) && count($qs) > 0) {
            $questionsData = Questions_tab::whereIn('question_id', $qs)->limit($subject->question_limit)->inRandomOrder()->get();
        } else {
            $questionsData = [];
        }
        $questions = $questionsData;
        $title_button = "Save question";
        return view('subject.questions', compact('subject', 'title', 'answer', 'questions', 'title_button', 'difficultyLevels', 'questions_tab'));
    }


    public function postNewQuestion($id, QuestionRequest $req)
    {

        $subj = Subject::find($id);
        $quest = new Question($req->all());

        $subj->questions()->save($quest);
        session()->flash('flash_mess', 'Question was added successfully.');
        return redirect(route('subject.question', ['id' => $subj->id]));
    }

    public function postEditQuestion($id, QuestionRequest $req)
    {

        $question = Question::findOrFail($id);
        $question->update($req->all());

        session()->flash('flash_mess', 'Question #' . $question->id . ' was changed completely');
        return redirect(route('subject.question', $question->subject->id));
    }

    public function getDeleteQuestion($id)
    {

        $subj_id = Question::find($id)->subject->id;
        Question::destroy($id);
        session()->flash('flash_mess', 'Question #' . $id . ' was deleted');
        return redirect(route('subject.question', $subj_id));

    }

    public function getBeforeStartTest($id)
    {

        $subject = Subject::find($id);
        session()->forget('next_question_id');
        return view('subject.start', compact('subject'));
    }

    public function getStartTest($id)
    {

        $subject = Subject::find($id);
        $qs = \App\CatQuestion::where('category_id', $subject->category_id)->pluck('question_id')->toArray();
        if (!empty($qs) && count($qs) > 0) {
            $questionsData = Questions_tab::whereIn('question_id', $qs)->limit($subject->question_limit)->get();
        } else {
            $questionsData = [];
        }
        $questions = $questionsData;

//        $questions = $subject->questions()->orderBy('question_id', 'asc')->get();
        $first_question_id = $questions->min('question_id');
        $last_question_id = $questions->max('question_id');

        $duration = $subject->duration;
//        if (session('next_question_id')) {
//            $current_question_id = session('next_question_id');
//        } else {
        $current_question_id = $first_question_id;
        session(['next_question_id' => $current_question_id]);
//        }
        //dd($current_question_id);
        return view('subject.test', compact('subject', 'questions', 'current_question_id', 'first_question_id', 'last_question_id', 'duration'));
    }


    public function postSaveQuestionResult($id, Request $req)
    {
        //save result

        $subject = Subject::find($id);
        $question = Questions_tab::where('question_id', $req->get('question_id'))->first();
        //save the answer into table
        $duration = $subject->duration * 60;
        $time_taken = ((int)$req->get('time_taken' . $question->question_id));
        $time_per_question = $duration - $time_taken;


        if ($question->question_type == 1 || $question->question_type == 3) {
            if ($question->Answer == $req->get('option')) {
                Progression::create([
                    'user_id' => Auth::user()->id,
                    'question_id' => $req->get('question_id'),
                    'difficulty_level_id' => $question->difficulty_level_id,
                    'points' => $question->difficultyLevel->points,
                ]);
            }

        }
        $count = 0;
        if ($question->question_type == 2) {
            if ($question->answerA == $req->get('answerA')) {
                $count++;
            }
            if ($question->answerB == $req->get('answerB')) {
                $count++;

            }

            if ($count == 2) {
                Progression::create([
                    'user_id' => Auth::user()->id,
                    'question_id' => $req->get('question_id'),
                    'difficulty_level_id' => $question->difficulty_level_id,
                    'points' => $question->difficultyLevel->points,
                ]);
            } else if ($count == 1) {
                Progression::create([
                    'user_id' => Auth::user()->id,
                    'question_id' => $req->get('question_id'),
                    'difficulty_level_id' => $question->difficulty_level_id,
                    'points' => $question->difficultyLevel->points / 2,
                ]);
            }

        }


        $answer = new Answer();
        $answer->user_id = Auth::user()->id;
        $answer->question_id = $req->get('question_id');
        $answer->question_type = $req->get('question_type');
        $answer->subject_id = $id;


        if ($question->question_type == 1 || $question->question_type == 3) {
            $answer->user_answer = $req->get('option');
            $answer->question = $question->question;
            $answer->option1 = $question->optionA;
            $answer->option2 = $question->optionB;
            $answer->option3 = $question->optionC;
            $answer->option4 = $question->optionD;
            $answer->right_answer = $question->Answer;
        }

        if ($question->question_type == 2) {
            $answer->user_answerA = $req->get('answerA');
            $answer->user_answerB = $req->get('answerB');
            $answer->question = $question->question . ' ' . $question->question_other;
            $answer->option1 = $question->optionA;
            $answer->option2 = $question->optionB;
            $answer->answerA = $question->answerA;
            $answer->answerB = $question->answerB;
        }

        $answer->time_taken = $time_per_question;
        $answer->save();


//        $next_question_id = $subject->questions()->where('id', '>', $req->get('question_id'))->min('id');
        $next_question_id = Questions_tab::where('question_id', '>', $req->get('question_id'))->min('question_id');;


        if (!is_null($next_question_id)) {
            return Response::json(['next_question_id' => $next_question_id]);
        } else {
            return redirect()->route('result', [$id]);
//            return Response::json(['next_question_id' => null]);
        }
//        return redirect()->route('result', [$id]);
    }

    public function getShowResultOfSubjectForGuest($id)
    {

        $subject = Subject::findOrFail($id);
        $answers = Answer::whereSubjectId($id)->get();
        if ($answers->count()) {
            $cnt = $answers->count();
            $cnt_right_answ = 0;
            foreach ($answers as $a) {
                if ($a->user_answer == $a->right_answer)
                    $cnt_right_answ++;
            }

            $persetnages = ceil($cnt_right_answ * 100 / $cnt);
            $time_taken = date("H:i:s", strtotime(Answer::whereSubjectId($id)->orderBy('id', 'desc')->first()->time_taken));
            $title = 'Results of test';
            session()->flash('flash_mess', 'Your Exam data has been saved successfully');
            return view('subject.result', compact('subject', 'title', 'cnt', 'cnt_right_answ', 'persetnages', 'time_taken'));
        } else {
            return redirect('get/results');
        }
    }

    public function getAllSubjectsResults()
    {

        $title = 'Exams Results';

        $answers = null;
//        $answers = DB::table('answers as t1')->
//        select(DB::raw('
//                t1.*, t2.*,t3.*,
//                t2.name as username, t2.email as useremail, t3.name as subjectname,
//                SUM(IF(t1.user_answer=t1.right_answer,1,0))/(
//                SELECT COUNT(DISTINCT id)
//                FROM answers t1 GROUP BY subject_id)*100 AS porcent,
//                max(time_taken) as time
//            '))
//            ->leftJoin('users as t2', function ($join) {
//                $join->on('t1.user_id', '=', 't2.id');
//            })
//            ->leftJoin('subjects as t3', function ($join) {
//                $join->on('t1.subject_id', '=', 't3.id');
//            })->groupBy('t1.subject_id')->get();


        return view("subject.results", compact('title', 'answers'));
    }

    public function getUpdateQuestionResult()
    {

    }

    public function postUpdateQuestionResult()
    {

    }

    public function getResults()
    {
        if (auth()->user()->roles[0]->name == 'teacher') {
            $title = 'Class Exams Results';
            $class = Classes::where('class_user_id', \auth()->user()->id)->pluck('id')->toArray();
            $allsubject = Subject::whereIn('class_id', $class)->with(['answers' => function ($q) {
//                $q->where('user_id', auth()->user()->id);
            }])->get();
            return view("teacher.subject.user-results", compact('title', 'allsubject'));
        } else {
            $title = 'Exams Results';
            $allsubject = Subject::with(['answers' => function ($q) {
                $q->where('user_id', auth()->user()->id);
            }])->get();
            return view("subject.user-results", compact('title', 'allsubject'));
        }


    }


    public function getProgressionLevel()
    {
        $title = "Student Progression Level";

        $classes = Classes::where('class_user_id', \auth()->user()->id)->with(['users' => function ($query) {
            $query->groupBy('user_id');
        }])->get();
        $progessionLevel = [];
        $usersArray = [];
        if (!empty($classes) && count($classes) > 0) {
            foreach ($classes as $class) {
                if (!empty($class->users) && count($class->users) > 0) {
                    foreach ($class->users as $user) {

                        $level = Progression::where('user_id', $user->id)->sum('points');
                        $user1 = User::where('id', $user->id)->first();
                        array_push($progessionLevel, $level);
                        array_push($usersArray, $user1);
                    }
                }
            }
        }
        return view("teacher.progressionLevel.index", compact('title', 'classes', 'progessionLevel', 'usersArray'));
    }

    public function getUserProgressionLevel()
    {
        $title = "Student Progression Level";

        $classes = Classes::with(['users' => function ($query) {
            $query->where('user_id', \auth()->user()->id)->groupBy('user_id');
        }])->get();
        $progessionLevel = [];
        $usersArray = [];
        if (!empty($classes) && count($classes) > 0) {
            foreach ($classes as $class) {
                if (!empty($class->users) && count($class->users) > 0) {
                    foreach ($class->users as $user) {

                        $level = Progression::where('user_id', $user->id)->sum('points');
                        $user1 = User::where('id', $user->id)->first();
                        array_push($progessionLevel, $level);
                        array_push($usersArray, $user1);
                    }
                }
            }
        }
        return view("user.progressionLevel.index", compact('title', 'classes', 'progessionLevel', 'usersArray'));
    }





    /**
     * Exporting exams results into Excel
     */

    /**
     * user messaging
     */
}

