<?php

namespace App\Http\Controllers;

use App\CatQuestion;
use App\Questions_tab;
use App\DifficultyLevel;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class QuestionsController extends Controller
{
    public function index()
    {
        $questions = Questions_tab::orderBy('question_id', 'DESC')->get();
        $title = 'Questions Listing';
        return view('teacher.questions.index', compact('title', 'questions'));
    }

    public function createnew()
    {

        $title = 'Questions Creation';
        $difficultyLevels = DifficultyLevel::all();
        $categories = Category::all();
        return view('teacher.questions.create', compact('title', 'difficultyLevels', 'categories'));

    }

    public function storequestion(Request $request)
    {
        $this->validate($request, [
            'question' => 'required',
            'answer' => Rule::requiredIf(function () use ($request) {
                if ($request['questiontype'] == "1" || $request->questiontype == "3") {
                    return true;
                }
            }),
            'answerA' => Rule::requiredIf(function () use ($request) {
                if ($request['questiontype'] == "2") {
                    return true;
                }
            }),
            'answerB' => Rule::requiredIf(function () use ($request) {
                if ($request['questiontype'] == "2") {
                    return true;
                }
            }),
            'optiona' => 'required',
            'optionb' => 'required',
            'questiontype' => 'required',
            'difficulty_level_id' => 'required'
        ], [
            'question.required' => 'Question is required.',
            'answer.required' => 'Answer is required.',
            'optiona.required' => 'Option A is required.',
            'optionb.required' => 'Option B is required.',
            'questiontype.required' => 'Question type is required.',
            'difficulty_level_id.required' => 'Difficulty Level is required.',
        ]);

        $questions = new Questions_tab();


        $questions->question = $request->question;
        $questions->optionA = $request->optiona;
        $questions->optionB = $request->optionb;


        if ($request->questiontype == "1" || $request->questiontype == "3") {
            $questions->Answer = $request->answer;
            $questions->optionC = $request->optionc;
            $questions->optionD = $request->optiond;
        }
        if ($request->questiontype == "2") {
            $questions->question_other = $request->question_other;
            $questions->answerA = $request->answerA;
            $questions->answerB = $request->answerB;
        }

        $questions->question_type = $request->questiontype;
        $questions->difficulty_level_id = $request->difficulty_level_id;
        $questions->created_at = date("Y-m-d H:i:s");
        $questions->updated_at = date("Y-m-d H:i:s");
        $questions->status = 'Active';

        //Handle file upload
        if ($request->hasFile('uploadfile')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('uploadfile')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('uploadfile')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload image
            $path = $request->file('uploadfile')->storeAs('public/images', $fileNameToStore);
            $questions->graph_image = $fileNameToStore;
        }
        $questions->save();

        if (!empty($request->categories) && count($request->categories) > 0) {
            foreach ($request->categories as $cat) {
                $category = new CatQuestion();
                $category->category_id = $cat;
                $category->question_id = $questions->question_id;
                $category->save();
            }
        }
        session()->flash('flash_mess', 'Question created successfully');
        return redirect()->route('questions.index');


    }


    public function editQuestion($id)
    {
        $title = 'Edit Question';
        $difficultyLevels = DifficultyLevel::all();
        $categories = Category::all();
        $question = Questions_tab::where('question_id', $id)->first();
        $categoriesCheck = \App\CatQuestion::where('question_id', $question->question_id)->pluck('category_id')->toArray();
        if (!empty($categoriesCheck) && count($categoriesCheck) > 0) {
            $categoriesData = \App\Category::whereIn('id', $categoriesCheck)->get();
        } else {
            $categoriesData = [];
        }
        return view('teacher.questions.edit', compact('title', 'difficultyLevels', 'question', 'categories', 'categoriesData'));

    }

    public function updateQuestion(Request $request, $id)
    {
        $this->validate($request, [
            'question' => 'required',
            'answer' => Rule::requiredIf(function () use ($request) {
                if ($request['questiontype'] == "1" || $request->questiontype == "3") {
                    return true;
                }
            }),
            'answerA' => Rule::requiredIf(function () use ($request) {
                if ($request['questiontype'] == "2") {
                    return true;
                }
            }),
            'answerB' => Rule::requiredIf(function () use ($request) {
                if ($request['questiontype'] == "2") {
                    return true;
                }
            }),
            'optiona' => 'required',
            'optionb' => 'required',
            'questiontype' => 'required',
            'difficulty_level_id' => 'required'
        ], [
            'question.required' => 'Question is required.',
            'answer.required' => 'Answer is required.',
            'optiona.required' => 'Option A is required.',
            'optionb.required' => 'Option B is required.',
            'questiontype.required' => 'Question type is required.',
            'difficulty_level_id.required' => 'Difficulty Level is required.',
        ]);


        $questions = Questions_tab::where('question_id', $id)->first();
        //Handle file upload
        if ($request->hasFile('uploadfile')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('uploadfile')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('uploadfile')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload image
            $path = $request->file('uploadfile')->storeAs('public/images', $fileNameToStore);
        } else {
            $fileNameToStore = $questions->graph_image;
        }

        $questions->question = $request->question;
        $questions->optionA = $request->optiona;
        $questions->optionB = $request->optionb;


        if ($request->questiontype == 1 || $request->questiontype == 3) {
            $questions->Answer = $request->answer;
            $questions->optionC = $request->optionc;
            $questions->optionD = $request->optiond;
        }
        if ($request->questiontype == 2) {
            $questions->question_other = $request->question_other;
            $questions->answerA = $request->answerA;
            $questions->answerB = $request->answerB;
        }

        $questions->question_type = $request->questiontype;
        $questions->difficulty_level_id = $request->difficulty_level_id;
        $request->graph_image = $fileNameToStore;
        $questions->save();

        if (!empty($request->categories) && count($request->categories) > 0) {
            foreach ($request->categories as $cat) {
                $category = CatQuestion::where('category_id', $cat)->first();
                if (!is_null($category)) {
                    $category->category_id = $cat;
                    $category->question_id = $id;
                    $category->save();
                } else {
                    $categorynew = new CatQuestion();
                    $categorynew->category_id = $cat;
                    $categorynew->question_id = $id;
                    $categorynew->save();
                }


            }
        }
        session()->flash('flash_mess', 'Question updated successfully');
        return redirect()->route('questions.index');


    }

}
