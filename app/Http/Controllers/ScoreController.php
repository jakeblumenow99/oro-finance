<?php

namespace App\Http\Controllers;

use App\Points;
use Illuminate\Http\Request;

class ScoreController extends Controller
{
    public function index()
    {
        $scores = Points::orderBy('id', 'DESC')->paginate(5);
        $title = 'Score Listing';
        return view('score.index', compact('scores', 'title'));
    }

    public function create()
    {
        $title = "Create New Score";

        return view('score.new',compact('title'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'points' => 'required',
        ], [
            'title.required' => 'Title is required.',
            'points.required' => 'Points is required.',
        ]);

        $score = new Points();
        $score->title = $request->title;
        $score->points = $request->points;
        $score->save();
        session()->flash('flash_mess', 'Score created successfully');
        return redirect()->route('score.index');

    }

    public function edit($id)
    {
        $score = Points::find($id);
        $title = "Edit Score '{$score->title}'";
        return view('score.edit', compact('score','title'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'points' => 'required',
        ], [
            'title.required' => 'Title is required.',
            'points.required' => 'Points is required.',
        ]);

        $score = Points::find($id);
        $score->title = $request->title;
        $score->points = $request->points;
        $score->save();
        session()->flash('flash_mess', 'Score Updated successfully');
        return redirect()->route('score.index');


    }

    public function destroy($id)
    {

        $score = Points::findOrFail($id);
        $score->delete();

        return redirect()->route('score.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Score has been deleted'
            ]);
    }
}
