<?php

namespace App\Http\Controllers;

use App\Classes;
use App\ClassUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ClassesController extends Controller
{
    public function index()
    {
        $classes = Classes::orderBy('id', 'DESC')->paginate(5);
        $title = 'Class Listing';
        return view('teacher.class.index', compact('classes', 'title'));
    }

    public function create()
    {
        $title = "Create New Class";
        $users = User::whereHas('roles', function ($q) {
            $q->where('name', 'guest');
        })->get();

        return view('teacher.class.new', compact('users', 'title'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'users' => 'required',
            'status' => 'required',
            'description' => 'required'
        ], [
            'name.required' => 'Name is required.',
            'slug.required' => 'Slug is required.',
            'users.required' => 'User is required.',
            'status.required' => 'Stauts is required.',
            'description.required' => 'Description is required.',
        ]);

        $class = new Classes();
        $class->name = $request->name;
        $class->slug = $request->slug;
        $class->description = $request->description;
        $class->status = $request->status;
        $class->class_user_id = auth()->user()->id;
        $class->save();
        $class->users()->attach($request->users);
        session()->flash('flash_mess', 'Class created successfully');
        return redirect()->route('class.index');

    }

    public function edit($id)
    {
        $class = Classes::find($id);
        $title = "Edit Class '{$class->name}'";
        $users = User::whereHas('roles', function ($q) {
            $q->where('name', 'guest');
        })->get();
        return view('teacher.class.edit', compact('class', 'users', 'title'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'users' => 'required',
            'description' => 'required',
            'status' => 'required'
        ], [
            'name.required' => 'Name is required.',
            'slug.required' => 'Slug is required.',
            'status.required' => 'Status is required.',
            'users.required' => 'User is required.',
            'description.required' => 'Descrtipion is required.',
        ]);

        $class = Classes::find($id);
        $class->name = $request->name;
        $class->slug = $request->slug;
        $class->description = $request->description;
        $class->status = $request->status;
        $class->class_user_id = auth()->user()->id;
        $class->save();
        $class->users()->detach();
        $class->users()->attach($request->users);
        session()->flash('flash_mess', 'Class updated successfully');
        return redirect()->route('class.index');

    }

    public function destroy($id)
    {

        $class = Classes::findOrFail($id);
        $class->users()->detach();
        $class->delete();

        return redirect()->route('class.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Class has been deleted'
            ]);
    }

}
