<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Category;
use App\Http\Requests\CategoryRequest;

use Symfony\Component\HttpFoundation\Session\Flash;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::paginate(5);
        $title = 'Categories Listing';
        return view('category.index', compact('categories', 'title'));
    }

    public function create()
    {
        $title = "Create New Category";
        return view('category.new', compact('title'));
    }



    public function store(CategoryRequest $req)
    {

        Category::create($req->all());
        session()->flash('flash_mess', 'Category was created completely');
        return redirect(route('category.index'));
    }

    public function show(){

    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        //dd($category);
        $title = "Edit Category '{$category->name}'";
        return view('category.edit', compact('category', 'title'));
    }

    public function update($id, CategoryRequest $req)
    {

        if ($req->get('status') == null)
            $req['status'] = 0;

        //dd($req);
        $category = Category::findOrFail($id);
        $category->update($req->all());
        session()->flash('flash_mess', 'Category was changed completely');
        return redirect(route('category.edit', $category->id));
    }

    public function destroy($id)
    {
        Category::findOrFail($id)->delete();
        return redirect(route('category.index'));
    }
}
