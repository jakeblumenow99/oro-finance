<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['question','question_other', 'option1', 'option2', 'option3', 'option4', 'answer', 'difficulty_level_id'];

    public function subject()
    {
        return $this->belongsTo('App\Subject', 'subject_id', 'id');
    }

    public function difficultyLevel()
    {
        return $this->belongsTo(DifficultyLevel::class, 'difficulty_level_id', 'id');
    }


}
