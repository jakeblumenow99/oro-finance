<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions_tab extends Model
{
    protected $primaryKey = 'question_id';
    protected $fillable = [ 'question_type', 'question', 'graph_image', 'optionA', 'optionB', 'optionC', 'optionC', 'optionD', 'status', 'created_date', 'modified_date', 'Answer','answerA','answerB', 'difficulty_level_id'];

    public function difficultyLevel()
    {
        return $this->belongsTo(DifficultyLevel::class, 'difficulty_level_id', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(Questions_tab::class, 'cat_questions', 'question_id', 'category_id')->withTimestamps()->as('cat_questions');
    }

}
