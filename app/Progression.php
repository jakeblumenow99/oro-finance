<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progression extends Model
{
    protected $fillable = ['user_id', 'question_id', 'difficulty_level_id', 'points'];
}
