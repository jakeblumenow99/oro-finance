<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'status'];

    /* public function getStatusAttribute($value){
         return $this->attributes['status']?"Active":"Non";
     }*/

    public function subjects()
    {
        return $this->hasMany('App\Subject', 'category_id', 'id');
    }

    public function questions()
    {
        return $this->belongsToMany(Category::class, 'cat_questions', 'category_id', 'question_id')->withTimestamps()->as('cat_questions');
    }

}
