<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $fillable = ['name', 'slug', 'description', 'status','class_user_id'];


    public function users()
    {
        return $this->belongsToMany(User::class, 'class_users', 'class_id', 'user_id')->withTimestamps()->as('class_users');
    }
    public function subjects(){
        return $this->hasMany('App\Subject','class_id', 'id');
    }
}
