<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Contracts\Auth\Access\Gate;

Route::group(['middleware' => 'web'], function () {

    Route::auth();
    Route::get('/home', function () {
        return redirect('/');
    });

    Route::get('/student/dashboard', function () {
        $user = \Illuminate\Support\Facades\Auth::user();

        if (\Illuminate\Support\Facades\Auth::check()) {
            return view('subject.dashboard', compact('user'));
        } else {
            return redirect()->route('login');
        }

    })->name('student.dashboard');
    Route::get('/', 'HomeController@index');
    Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
        Route::get('/', function () {
            return view('admin.index');
        });
        Route::get('/user', function () {
            return view('admin.user');
        });

        Route::resource('category', 'CategoryController');
        Route::get('results', 'SubjectController@getAllSubjectsResults')->name('subjects.results');
        Route::get('subject', 'SubjectController@getIndex')->name('subject.index');
        Route::get('subject/new', 'SubjectController@getNew')->name('subject.new');
        Route::get('subject/{id}', 'SubjectController@getQuestions')->name('subject.question');
        Route::post('subject/{id}/edit', 'SubjectController@postEditQuestion')->name('subject.question.post.edit');
        Route::post('subject/new', 'SubjectController@postNewSubject')->name('subject.new');
        Route::post('subject/{id}', 'SubjectController@postNewQuestion')->name('subject.post.new.question');
        Route::get('subject/{id}/edit', 'SubjectController@getEdit')->name('subject.question.edit');
        Route::get('subject/{id}/delete', 'SubjectController@getDelete')->name('subject.delete');
        Route::get('subject/{id}/delete/question', 'SubjectController@getDeleteQuestion')->name('subject.question.delete');
        Route::patch('subject/{id}/edit', 'SubjectController@patchEdit')->name('subject.question.patch.edit');
        Route::resource('subject', 'SubjectController', ['except' => ['result', 'save-question', 'before-exam', 'start-exam']]);
        Route::resource('user', 'UserController');
        Route::resource('difficulty', 'DifficultyLevelController');
        Route::resource('score', 'ScoreController');
    });
    Route::group(['prefix' => 'user', 'middleware' => 'user'], function () {
        Route::get('/', function () {
            return redirect('user/get/results');
        })->name('index.home');
        Route::get('result/{id}', 'SubjectController@getShowResultOfSubjectForGuest')->name('result');
        Route::post('subject/save-question-result/{id}', 'SubjectController@postSaveQuestionResult')->name('user.save-question');
        Route::get('subject/{id}/start', 'SubjectController@getBeforeStartTest')->name('before-exam');
        Route::get('subject/{id}/start-test', 'SubjectController@getStartTest')->name('start-exam');
        Route::get('get/results', 'SubjectController@getResults')->name('get.results');
        Route::get('get/progression/level', 'SubjectController@getUserProgressionLevel')->name('user.get.progression.level');


    });
    Route::group(['prefix' => 'teacher', 'middleware' => 'teacher'], function () {


        Route::resource('category', 'CategoryController');
        Route::get('subject', 'SubjectController@getIndex')->name('subject.index');
        Route::get('subject/new', 'SubjectController@getNew')->name('subject.new');
        Route::get('subject/{id}', 'SubjectController@getQuestions')->name('subject.question');
        Route::post('subject/{id}/edit', 'SubjectController@postEditQuestion')->name('subject.question.post.edit');
        Route::post('subject/new', 'SubjectController@postNewSubject')->name('subject.new');
        Route::post('subject/{id}', 'SubjectController@postNewQuestion')->name('subject.post.new.question');
        Route::get('subject/{id}/edit', 'SubjectController@getEdit')->name('subject.question.edit');
        Route::get('subject/{id}/delete', 'SubjectController@getDelete')->name('subject.delete');
        Route::get('subject/{id}/delete/question', 'SubjectController@getDeleteQuestion')->name('subject.question.delete');
        Route::patch('subject/{id}/edit', 'SubjectController@patchEdit')->name('subject.question.patch.edit');
        Route::resource('subject', 'SubjectController', ['except' => ['result', 'save-question', 'before-exam', 'start-exam']]);
        Route::resource('class', 'ClassesController');
        Route::get('get/results', 'SubjectController@getResults')->name('teacher.get.results');

        Route::get('get/progression/level', 'SubjectController@getProgressionLevel')->name('teacher.get.progression.level');


        Route::get('questions/createnew', 'QuestionsController@createnew')->name('questions.createnew');
        Route::post('questions/storequestion', 'QuestionsController@storequestion')->name('questions.storequestion');
        Route::post('questions/updatequestion/{id}', 'QuestionsController@updateQuestion')->name('questions.updatequestion');
        Route::get('questions', 'QuestionsController@Index')->name('questions.index');
        Route::get('questions/edit/{id}', 'QuestionsController@editQuestion')->name('questions.edit');


    });
    Route::get('profile/edit', 'UserController@profileEdit')->name('user.profile.edit');
    Route::post('profile/edit', 'UserController@profileUpdate')->name('user.profile.update');
});





