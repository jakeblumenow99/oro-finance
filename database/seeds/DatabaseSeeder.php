<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'id' => 1,
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
        ]);
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'id' => 2,
            'name' => 'Student',
            'email' => 'user1@gmail.com',
            'password' => bcrypt('user1'),
        ]);
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'id' => 3,
            'name' => 'Teacher',
            'email' => 'teacher@gmail.com',
            'password' => bcrypt('teacher'),
        ]);


        \Illuminate\Support\Facades\DB::table('roles')->insert([
            'name' => 'admin',
            'label' => 'Admin',
            'id' => 1
        ]);
        \Illuminate\Support\Facades\DB::table('roles')->insert([
            'name' => 'guest',
            'label' => 'Guest',
            'id' => 2
        ]);

        \Illuminate\Support\Facades\DB::table('roles')->insert([
            'name' => 'teacher',
            'label' => 'Teacher',
            'id' => 3
        ]);
        \Illuminate\Support\Facades\DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 1
        ]);
        \Illuminate\Support\Facades\DB::table('role_user')->insert([
            'user_id' => 2,
            'role_id' => 2
        ]);

        \Illuminate\Support\Facades\DB::table('role_user')->insert([
            'user_id' => 3,
            'role_id' => 3
        ]);
    }
}
